##
# Copyright or © or Copr. National Center for Scientific Research or CNRS
# Contributors: Guillaume Brysbaert, Théo Mauri and Marc Lensink (2018)
# 
# Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
# 
# This software is a computer program (example case) whose purpose is to create 
# Residue Interaction Networks (RINs) from Chimera through the structureViz app 
# in Cytoscape for the PDB ID 2JZC without H bonds detection.
# 
# Tested with Cytoscape 3.6.1 and Python 3.5.2
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use, 
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
##


import requests
import time

requests.get('http://localhost:1234/v1/commands/structureViz/launch')
time.sleep(10)
requests.get('http://localhost:1234/v1/commands/structureViz/open?pdbID=2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.1')
time.sleep(10)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=01_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.2')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=02_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.3')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=03_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.4')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=04_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.5')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=05_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.6')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=06_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.7')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=07_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.8')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=08_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.9')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=09_2jzc')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/send?command=select%20%230.10')
time.sleep(15)
requests.get('http://localhost:1234/v1/commands/structureViz/createRIN?includeHBonds=false&networkName=10_2jzc')
