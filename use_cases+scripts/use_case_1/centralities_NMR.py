##
# Copyright or © or Copr. National Center for Scientific Research or CNRS
# Contributors: Guillaume Brysbaert, Théo Mauri and Marc Lensink (2018)
# 
# Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
# 
# This software is a computer program whose purpose is to run centrality analyses
# on Residue Interaction Networks (RINs) in Cytoscape through the
# automation functionalities provided by the cyREST core app and the RINspector app.
# It calculates a Z-score for each residue and gathers the results for all the RINs
# in one recap csv table.
# 
# Tested with Cytoscape 3.6.1 and Python 3.5.2
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use, 
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
##

import requests
import json
import os

import sys
import csv

#comment the methods not to be used between RCA, BCA and CCA
##RCA
typeOfCentrality = "Residue (ASPL change under removal of individual nodes) - RCA"
zscoreBaseName = "Z-score_RCA"
##BCA
#typeOfCentrality = "Betweenness - BCA"
#zscoreBaseName = "Z-score_BCA"
##CCA
#typeOfCentrality = "Closeness - CCA"
#zscoreBaseName = "Z-score_CCA"

#sets the output file name
outputFileName = zscoreBaseName+"_NMR.csv"

####################################################################################
################ Makes a list with all the SUIDs of all the Networks  ##############
################       to analyze all the Networks                    ##############
####################################################################################

listOfNetworks = requests.post('http://localhost:1234/v1/commands/network/list')
listOfNetworks = listOfNetworks.json()
listOfNetworks = (listOfNetworks['data']['networks'])

####################################################################################
################ Sets a list to print on the first row of the csv    ###############
################        with the name of the network                 ###############
####################################################################################

header=['Residue']

####################################################################################
################ Sets a hash table to keep the calculated results of each ##########
################       amino acid in the different networks               ##########
####################################################################################

dico = dict()
for i in listOfNetworks:
    
####################################################################################
################ Calculates the centralities #######################################
####################################################################################    

    w = requests.post('http://localhost:1234/v1/commands/network/set current',json={'network':'SUID:'+str(i)})
    r = requests.post('http://localhost:1234/v1/commands/network/get',json={'network':'current'})
    r=r.json()

    networkName=r['data']['name']

    calcul=requests.post('http://localhost:1234/v1/commands/rinspector/centrality',json={'analysisType':typeOfCentrality})
    
################ Gets the column with all the residues in the network ################   

    columnofRes = requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':'Residue','table':networkName+' default node'})

###############  Gets the column with the Z-scores (depends on the type of analysis) ################

    Zscore=requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':zscoreBaseName,'table':networkName+' default node'})

####################################################################################
################    Analyzes the different responses of the requests     ###########
################    Sets the name of the column in the list for      ###############
################            the first row of the csv file               ############
####################################################################################    

    rep=Zscore.json()
    typeOfAnalysis=rep['data']['name']
    columnName=typeOfAnalysis+"_"+networkName
# sets the column name here
    header.append(columnName)
    valuesOfZScore=(rep['data']['values'])
    Res=columnofRes.json()
    nameOfRes=(Res['data']['values'])
    
####################################################################################
################ All the Residues and Z-scores for each network are added   ########
################ to the dictionary with a \t to separate all the results    ########
####################################################################################     

    i=0
    while i < len(nameOfRes):
        
        val=nameOfRes[i]
        if (val in dico):
            dico[val] += str("\t"+str(valuesOfZScore[i]))
        else:
            dico[val] = str(valuesOfZScore[i])
        i=i+1
    print(networkName + ': done.')
    
####################################################################################
################ Creates the output file and fill it with all the   ################
################        results of the hash table                   ################
####################################################################################

with open(outputFileName, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter="\t",quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(x for x in header)
    for key in dico:
        listofScore=[]
        listofvalue=str(dico[key]).split('\t')
        listofScore.append(str(key))
        for i in listofvalue:
            listofScore.append(i)
        writer.writerow(x for x in listofScore)
    
csvfile.close()

print(outputFileName+" created.")
