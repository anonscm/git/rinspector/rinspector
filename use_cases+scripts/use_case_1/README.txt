To run the use case 1:

1) download Cytoscape 3.6.1 (or higher) from http://www.cytoscape.org
2) start Cytoscape
3) in Apps/App manager, search and install RINspector
4) close the App manager window
5) open the NMR_2JZC.cys file inside Cytoscape (File/Open)
6) run the centralities_NMR.R or centralities_NMR.py (for python 3)

The type of centrality can be changed as stated in the script files (default is "Residue (ASPL change under removal of individual nodes) - RCA").


