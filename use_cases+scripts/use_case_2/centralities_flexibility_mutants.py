##
# Copyright or © or Copr. National Center for Scientific Research or CNRS
# Contributors: Guillaume Brysbaert, Théo Mauri and Marc Lensink (2018)
# 
# Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
# 
# This software is a computer program whose purpose is to run centrality 
# analyses and DynaMine flexibility predictions on Residue Interaction 
# Networks (RINs) in Cytoscape through the automation functionalities 
# provided by the cyREST core app and the RINspector app.
# It calculates a Z-score for each residue and gathers the results for all
# the RINs in one recap csv table.
# It also runs DynaMine flexibility predictions for all the sequences associated 
# to the RINs, plots a recap chart for all the predictions and creates a 
# recap csv table with S² values for each residue.
#
# Tested with Cytoscape 3.6.1 and Python 3.5.2
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use, 
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
##

import requests
import json
import os

import sys
import csv
import matplotlib.pyplot as plt
from collections import OrderedDict


#comment the methods not to be used between RCA, BCA and CCA
##RCA
typeOfCentrality = "Residue (ASPL change under removal of individual nodes) - RCA"
zscoreBaseName = "Z-score_RCA"
##BCA
#typeOfCentrality = "Betweenness - BCA"
#zscoreBaseName = "Z-score_BCA"
##CCA
#typeOfCentrality = "Closeness - CCA"
#zscoreBaseName = "Z-score_CCA"

#sets the output file name
outputFileNameZ = zscoreBaseName+"_OGT_TPR.csv"
outputFileNameSS = "Flexibility_OGT_TPR.csv"
outputPNGFileNameSS = "Flexibility_OGT_TPR.png"

#for the graph
plt.figure(figsize=(16,7))
plt.title("DynaMine flexibility prediction")
plt.xlabel('Sequence Index')
plt.ylabel('S²')

####################################################################################
################ Makes a list with all the SUIDs of all the Networks  ##############
################       to analyze all the Networks                  ################
####################################################################################

listOfNetworks = requests.post('http://localhost:1234/v1/commands/network/list')
listOfNetworks = listOfNetworks.json()
listOfNetworks = (listOfNetworks['data']['networks'])

####################################################################################
################ Sets a list to print on the first row of the csv    ###############
################        with the name of the network                ################
####################################################################################

headerZscore=['ResIndex']
headerZscore.append('Residue')
headerSSscore=['ResIndex']
headerSSscore.append('Residue')

####################################################################################
################ Sets a hash table to keep the calculated results of each ##########
################       amino acid in the different networks               ##########
####################################################################################

dicoZscore=dict()
dicoSSscore=dict()
for i in listOfNetworks:

####################################################################################
################ Calculates the centralities #######################################
####################################################################################    

    w = requests.post('http://localhost:1234/v1/commands/network/set current',json={'network':'SUID:'+str(i)})
    r = requests.post('http://localhost:1234/v1/commands/network/get',json={'network':'current'})
    r=r.json()
    networkName=r['data']['name']

    calcul=requests.post('http://localhost:1234/v1/commands/rinspector/centrality',json={'analysisType':typeOfCentrality})
    
################ Gets the column with all the residues in the network ################  

    columnofRes = requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':'Residue','table':networkName+' default node'})

###############  Gets the column with the Z-scores (depends on the type of analysis) ################

    Zscore=requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':zscoreBaseName,'table':networkName+' default node'})
    
    index=requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':'ResIndex','table':networkName+' default node'})
    index=(index.json())
    index=index['data']['values']
    chain=requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':'ResChain','table':networkName+' default node'})
    chain=(chain.json())
    chain=chain['data']['values'][1]
    flexibility=requests.post('http://localhost:1234/v1/commands/rinspector/dynamine', json = {'chainToAnalyze':chain})
    scoreSS=requests.post('http://localhost:1234/v1/commands/table/get column',json={'column':'S²','table':networkName+' default node'})
    score=scoreSS.json()

    SS=score['data']['values']

####################################################################################
################        Gets all the information after the          ################
################   calculations of centralities and flexibilities    ###############
####################################################################################

    j=0
    dic=dict()
    while j < len(SS):
        val=str(index[j])
        if (val in dic):
            dic[val] += str(SS[j])
        else:
            dic[val] = str(SS[j])
        j=j+1
    dic= OrderedDict(sorted(dic.items(), key=lambda t: t[0]))
    listOfIndex=[]
    listOfSS=[]
    
    for k in dic:
        listOfIndex.append(float(k))
        listOfSS.append(float(dic[k]))
		
 ###############  Plots the S² score for each network  ################
    
    plt.plot(listOfIndex,listOfSS,label=networkName)
    
####################################################################################
################    Analyzes the different responses of the requests     ###########
################    Sets the name of the column in the list for      ###############
################            the first row of the csv file               ############
####################################################################################      

    rep=Zscore.json()
    typeOfAnalysis=rep['data']['name']
    columnNameZ=typeOfAnalysis+"_"+networkName
    headerZscore.append(columnNameZ) # set  the column name here
    columnNameS="S²_"+networkName
    headerSSscore.append(columnNameS)
    valuesOfZScore=(rep['data']['values'])
    Res=columnofRes.json()
    nameOfRes=(Res['data']['values'])
    
####################################################################################
################    All the Residues, Z-scores & S² scores for each ################
################       network are added to the dictionary with     ################
################          a \t to separate all the results          ################
####################################################################################     

    j=0
    while j < len(nameOfRes):
        
        valZ=index[j]
        if (valZ in dicoZscore):
            dicoZscore[valZ] += str("\t"+str(valuesOfZScore[j]))
        else:
            dicoZscore[valZ] = nameOfRes[j]+"\t"+str(valuesOfZScore[j])
        
        valS=index[j]
        if (valS in dicoSSscore):
            dicoSSscore[valS] += "\t"+str(SS[j])
        else:
            dicoSSscore[valS] = nameOfRes[j]+"\t"+str(SS[j])
        j=j+1
        
    print(networkName + ': done.')
    
####################################################################################
################ Creates the output file and fill it with all the   ################
################            results for the centralities            ################
####################################################################################  

    with open(outputFileNameZ, 'w', newline='') as Zcsvfile:
        writer = csv.writer(Zcsvfile, delimiter="\t",quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(x for x in headerZscore)
        for key in dicoZscore:
            listofScore=[]
            listofScore.append(str(key))
            listofvalue=str(dicoZscore[key]).split('\t')
            for l in listofvalue:
                listofScore.append(l)
            writer.writerow(x for x in listofScore)
    

####################################################################################
################ Creates the output file and fill it with all the   ################
################            results for the flexibilities           ################
####################################################################################

    with open(outputFileNameSS, 'w', newline='') as SScsvfile:
        writer = csv.writer(SScsvfile, delimiter="\t",quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(x for x in headerSSscore)
        for key in dicoSSscore:
            listofScore=[]
            listofScore.append(str(key))
            listofvalue=str(dicoSSscore[key]).split('\t')
            for l in listofvalue:
                listofScore.append(l)
            writer.writerow(x for x in listofScore)
    

####################################################################################
################         Creates the graph of flexibilities         ################
####################################################################################

Zcsvfile.close() 
SScsvfile.close() 

firstValueForAbscisse=(listOfIndex[0])
lastValueForAbscisse=(listOfIndex[-1])
plt.legend()
plt.plot([firstValueForAbscisse,lastValueForAbscisse], [0.8, 0.8], 'grey', lw=2)
plt.plot([firstValueForAbscisse,lastValueForAbscisse], [0.69, 0.69], 'grey', lw=2)
plt.savefig(outputPNGFileNameSS)

print(outputFileNameZ+" created.")
print(outputFileNameSS+" created.")
print(outputPNGFileNameSS+" created.")
