To run the use case 2:

1) download Cytoscape 3.6.1 (or higher) from http://www.cytoscape.org
2) start Cytoscape
3) in Apps/App manager, search and install RINspector (if necessary)
4) close the App manager window
2) open the OGT_TPRs_4GYW.cys file inside Cytoscape (file/open)
6) run the centralities_flexibility_mutants.R or centralities_flexibility_mutants.py (for python 3)

The type of centrality can be changed as stated in the script files (default is "Residue (ASPL change under removal of individual nodes) - RCA").


