package org.cytoscape.app.RINspector.internal.task.DynaMine;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import org.cytoscape.app.RINspector.internal.task.tools.RINUtils;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.PassthroughMapping;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.json.JSONResult;
import org.cytoscape.work.util.ListSingleSelection;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleAnchor;

/**
 * Class that permits to perform DynaMine predictions
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class DynaMineAnalysis extends AbstractTask implements RowsSetListener, ObservableTask {

	// global Cytoscape Variables and tools
	private TaskMonitor tm;
	private final CyApplicationManager appmanag;
	private final VisualMappingManager vmm;
	private final VisualStyleFactory visFactFactory;
	private final VisualMappingFunctionFactory visMapFact;
	private final VisualMappingFunctionFactory vmfFactoryP;
	private final CyServiceRegistrar registrar;
	private final CySwingApplication cytoscapeDesktopService;

	private String sequence;
	private final String json_key;
	private final HashMap<String, Character> aa = new HashMap<String, Character>(22);
	private final String[] color = { "black", "blue", "cyan", "darkGray", "gray", "lightGray", "magenta", "orange",
			"pink", "red" };

	private JFreeChart lineChart;
	private String lineChartNetworkName;
	private final GridBagLayout gridlayout = new GridBagLayout();
	private GridBagConstraints gridConstraints = new GridBagConstraints();
	private int nbseq = 0;
	private String seqName = "Original";
	private JLabel loading;
	private JTable tableRes;
	private String lastUrlResults;
	private ArrayList<String> urlResults = new ArrayList<String>();
	private String chain;
	private int[] resIndexSeq;
	// selected residues
	private ArrayList<String> selectedResidues;

	private final BoundaryRangeValues<Paint> brv2 = new BoundaryRangeValues<Paint>(Color.RED, Color.GREEN, Color.GREEN);
	private final BoundaryRangeValues<Paint> brv3 = new BoundaryRangeValues<Paint>(Color.GREEN, Color.GREEN,
			new Color(70, 170, 255));

	// for visual style
	private final String DynaMineS2StyleName = "DynaMine_S2";
	private final String DynaMineS2ColumnName = "S²";

	private final double nodeWidth = 80.0d;
	private final double nodeHeight = 40.0d;
	private final int labelSize = 20;
	private final int edgeTransparency = 150;

	public PlotRenderingInfo plotRender;
	
	//associates SUID with S²
	private HashMap<Long, Double> S2Score;

	@Tunable(description = "Select the chain to for prediction ", groups = { "Chain" }, longDescription="Chain for prediction among the chains available in the RIN (from ResChain column, e.g. \"A\") ", context=Tunable.BOTH_CONTEXT)
	public ListSingleSelection<String> chainToAnalyze = new ListSingleSelection<String>(getChains());
	
	public ArrayList<String> getChains() {
		if(DynaMineAnalysisFactory.chains.size() <= 0)
		{
			ArrayList<String> al = new ArrayList<String>();
			al.add(new String());
			return al;
		}
		else
		{
			return DynaMineAnalysisFactory.chains;
		}
	}
	
    public void setChain(String chainToAdd) {
	       DynaMineAnalysisFactory.chains.add(chainToAdd);
	}
	
	/**
	 * Constructor of DynaMineAnalysis object
	 * 
	 * @param registrar
	 * @param cytoscapeDesktopService
	 * @param cyApplicationManager
	 * @param vmm
	 * @param visFactFactory
	 * @param visMapFact
	 * @param vmfFactoryP
	 * @param json_key
	 */
	public DynaMineAnalysis(CyServiceRegistrar registrar, CySwingApplication cytoscapeDesktopService,
			CyApplicationManager cyApplicationManager, VisualMappingManager vmm, VisualStyleFactory visFactFactory,
			VisualMappingFunctionFactory visMapFact, VisualMappingFunctionFactory vmfFactoryP, String json_key) {
		this.appmanag = cyApplicationManager;
		this.json_key = json_key;
		this.vmm = vmm;
		this.visFactFactory = visFactFactory;
		this.visMapFact = visMapFact;
		this.vmfFactoryP = vmfFactoryP;
		this.registrar = registrar;
		this.cytoscapeDesktopService = cytoscapeDesktopService;

		RINUtils.initHashMap(aa);
	}

	/**
	 * Create sequence from ResType and ResIndex column
	 * 
	 * @param network
	 * @return
	 */
	public String networkSequence(CyNetwork network) {
		String sequence = "";
		int[] cyIndex = new int[network.getNodeCount()];
		int i = 0;

		for (int j = 0; j < cyIndex.length; j++) {
			cyIndex[j] = -1;
		}
		// get all resIndex from Cytable
		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			if (row.get("ResChain", String.class).equals(this.chain)) {
				cyIndex[i] = row.get("ResIndex", Integer.class);
				i++;
			}
		}
		Arrays.sort(cyIndex); // sort index to have the first index in the first
								// box

		int initStart = 0;
		for (int j = 0; j < cyIndex.length; j++) {
			if (cyIndex[j] == -1)
				initStart++;
		}
		int x = 0;
		this.resIndexSeq = new int[cyIndex.length - initStart];
		for (int j = initStart; j < cyIndex.length; j++) {
			this.resIndexSeq[x] = cyIndex[j];
			x++;
		}

		// put all 1-letter code residue in the table
		char[] sequenceTab = new char[cyIndex[cyIndex.length - 1]];
		for (int j = 0; j < sequenceTab.length; j++)
			sequenceTab[j] = 'Z';
		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			if (row.get("ResChain", String.class).equals(this.chain)) {
				String res = (row.get("ResType", String.class)).toUpperCase();
				int index = row.get("ResIndex", Integer.class);
				if (aa.containsKey(res))
					sequenceTab[index - 1] = aa.get(res);
				else
					sequenceTab[index - 1] = 'X';
			}
		}

		StringBuffer result = new StringBuffer();
		for (int j = 0; j < sequenceTab.length; j++) {
			if (sequenceTab[j] != 'Z')
				result.append(sequenceTab[j]);
		}

		sequence = result.toString();
		return sequence.trim();
	}

	/**
	 * Return the current status of DynaMine jod_id
	 * 
	 * @param job_id
	 *            DynaMine jod id
	 * @return Status
	 */
	public static String pollResults(String job_id) {
		String urlParam = "batch=%7B%22json_api_key%22%3A+%2234b3cfa367fea4efda0827564d64b194011db04287760641935e6df7%22%2C+%22protocol%22%3A+%221.0%22%2C+%22job_id%22%3A+%22"
				+ job_id + "%22%7D";

		return RINUtils.executePost("http://dynamine.ibsquare.be/batch_request", urlParam);
	}

	/**
	 * Submit job to DynaMine batch request server
	 * 
	 * @param urlParam
	 *            url post parameters
	 * @return response from DynaMine server
	 * @throws InterruptedException
	 */
	public String submitJob(String urlParam) throws InterruptedException {
		String response = RINUtils.executePost("http://dynamine.ibsquare.be/batch_request", urlParam); // format
		String status = response.substring(response.indexOf(":") + 3, response.indexOf(",") - 1);
		if (status.equals("error")) {
			// display the error message
			tm.setTitle(response.substring(response.lastIndexOf(":") + 3, response.lastIndexOf("\"")));
			return null;
		}
		// retrieve the job id
		String jobId = response.substring(response.indexOf("d\":") + 5, response.lastIndexOf("\""));
		RINUtils.printProgress(status, false, tm);
		while (!status.equals("completed")) {
			Thread.sleep(1500);
			response = pollResults(jobId);
			status = response.substring(response.indexOf(":") + 3, response.indexOf(",") - 1);
			if (status.equals("error")) {
				tm.setTitle(response.substring(response.indexOf("e\":") + 5, response.lastIndexOf("\"")));
				return null;
			}
			RINUtils.printProgress(status, true, tm);
		}
		return response;
	}

	/**
	 * Extract result link from DynaMine response
	 * 
	 * @param dynaResp
	 *            DynaMine string response
	 * @return url of zip archive containing results of analyze
	 */
	public String extractResults(String dynaResp) {
		String url = dynaResp.substring(dynaResp.indexOf("\"url\"") + 8, dynaResp.indexOf(".zip\"") + 4);
		return url;
	}

	/**
	 * Extract sValues from DynaMine response
	 * 
	 * @param dynaResp
	 *            DynaMine string response
	 * @param nbNodes
	 *            number of node in the network
	 * @return tab containing residue associate with their sValue
	 */
	public ArrayList<ArrayList<String>> sValues(String dynaResp, int nbNodes) {
		ArrayList<ArrayList<String>> svalues = new ArrayList<ArrayList<String>>();
		String values = dynaResp.substring(dynaResp.indexOf("[[") + 1, dynaResp.lastIndexOf("]"));
		for (int i = 0; i < nbNodes; i++) {
			ArrayList<String> couple = new ArrayList<String>();
			couple.add(values.substring(values.indexOf("\"") + 1, values.indexOf("\"") + 2));
			couple.add(values.substring(values.indexOf(",") + 2, values.indexOf("]")));
			if (i < nbNodes - 1)
				values = values.substring(values.indexOf("]") + 3);
			svalues.add(couple);
		}
		return svalues;
	}

	/**
	 * Add S column to CyNetwork table
	 * 
	 * @param network
	 * @param values
	 *            array containing residues associated with their s value
	 */
	public void addSColumn(CyNetwork network, ArrayList<ArrayList<String>> values) {
		tm.setTitle("Adding S² column");

		// create column S²
		String attributeName = "S²";
		if (network.getDefaultNodeTable().getColumn("S²") == null)
			network.getDefaultNodeTable().createColumn(attributeName, Double.class, false);

		// get all residues sorted by their ResIndex
		int[] cyIndex = new int[network.getNodeCount()];
		int i = 0;
		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			if (row.get("ResChain", String.class).equals(this.chain)) {
				cyIndex[i] = row.get("ResIndex", Integer.class);
				i++;
			}
		}
		Arrays.sort(cyIndex);

		CyNode[] sequenceTab = new CyNode[cyIndex[cyIndex.length - 1]];
		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			if (row.get("ResChain", String.class).equals(this.chain)) {
				int index = row.get("ResIndex", Integer.class);
				sequenceTab[index - 1] = node;
			}
		}

		//initialization of HashMap
		S2Score = new HashMap<Long, Double>(sequenceTab.length);
		
		int x = 0;
		for (int j = 0; j < sequenceTab.length; j++) {
			if (sequenceTab[j] != null) {
				CyNode node = sequenceTab[j];
				CyRow row = network.getRow(node);

				ArrayList<String> couple = values.get(x);
				//add the S² value to the row in the table and add SUID and S² to the HashMap
				double s2double = Double.parseDouble(couple.get(1));
				S2Score.put(node.getSUID(),s2double);
				row.set(attributeName, s2double);
				x++;
			}
		}
	}

	/**
	 * Create DynaMine style in Cytoscape view
	 * 
	 * @param values
	 *            values of Svalues
	 */
	public void createDynaMineStyle(ArrayList<ArrayList<String>> values, String styleName) {
		tm.setTitle("Creating DynaMine Style...");

		// if style already exists, remove it to create the new one
		Iterator<VisualStyle> it = vmm.getAllVisualStyles().iterator();
		while (it.hasNext()) {
			VisualStyle curVS = it.next();
			if (curVS.getTitle().equals(styleName)) {
				vmm.setCurrentVisualStyle(curVS);
				return;
			}
		}

		CyNetworkView myView = this.appmanag.getCurrentNetworkView();
		VisualStyle vs = visFactFactory.createVisualStyle(vmm.getVisualStyle(myView));
		vs.setTitle(styleName);

		// *******************************************************
		// Define node
		// fill color
		// ********************************************************
		vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.GRAY);

		// Set node color map to attribute "S²"
		ContinuousMapping<Double, Paint> mappingColor = (ContinuousMapping<Double, Paint>) this.visMapFact
				.createVisualMappingFunction(DynaMineS2ColumnName, Double.class, BasicVisualLexicon.NODE_FILL_COLOR);

		// Define the points
		double val2 = 0.7d;
		double val3 = 0.8d;

		// Set the points
		mappingColor.addPoint(val2, brv2);
		mappingColor.addPoint(val3, brv3);

		// add the mapping to visual style
		vs.addVisualMappingFunction(mappingColor);

		// **************************************************************
		// Define
		// node
		// label
		// ***************************************************************
		PassthroughMapping<String, ?> mappingLabel = null;
		if (this.appmanag.getCurrentNetwork().getDefaultNodeTable().getColumn("Residue") != null) {
			mappingLabel = (PassthroughMapping<String, ?>) this.vmfFactoryP.createVisualMappingFunction("Residue",
					String.class, BasicVisualLexicon.NODE_LABEL);
		} else {
			mappingLabel = (PassthroughMapping<String, ?>) this.vmfFactoryP.createVisualMappingFunction("name",
					String.class, BasicVisualLexicon.NODE_LABEL);
		}

		vs.addVisualMappingFunction(mappingLabel);

		// **************************************************************
		// Define
		// label font
		// style
		// **************************************************************
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_LABEL_FONT_FACE);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_LABEL_FONT_SIZE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_FACE, new Font("Arial", Font.PLAIN, 20));
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, labelSize);

		// **************************************************************
		// Define
		// node shape
		// **************************************************************
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_SHAPE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);

		// ***************************************************************
		// Define
		// node
		// size
		// ***************************************************************
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_WIDTH);
		vs.setDefaultValue(BasicVisualLexicon.NODE_WIDTH, this.nodeWidth);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_HEIGHT);
		vs.setDefaultValue(BasicVisualLexicon.NODE_HEIGHT, this.nodeHeight);

		// ***************************************************************
		// Define default style parameters
		// ********************************************************************
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_SHAPE);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_LABEL_COLOR);
		vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_TRANSPARENCY);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_SELECTED_PAINT);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_PAINT);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_WIDTH);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_TRANSPARENCY, this.edgeTransparency);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SELECTED_PAINT, Color.YELLOW);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_PAINT, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, new Double(2));

		// **************************************************************
		// Adapt
		// current
		// view
		// **************************************************************

		// Add visual style to visual style manager
		vmm.addVisualStyle(vs);
		// set current style to RCA style
		vmm.setCurrentVisualStyle(vs);
		// updating network view
		myView.updateView();
	}

	/**
	 * Add all components to Result CyPanel
	 * 
	 * @param panel
	 *            CytoPanel contained in Result CyPanel
	 * @param size
	 *            number of line of the JTable to include
	 */
	public void addResultPanelComponent(MyCytoPanel panel, int size) {
		// adding separator between graph and other components
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 2;
		gridConstraints.weightx = 0;
		gridConstraints.weighty = 0;
		gridConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridConstraints.insets = new Insets(10, 0, 10, 0);
		panel.add(new JSeparator(SwingConstants.HORIZONTAL), gridConstraints);

		// adding JTable
		JTable table = new JTable(size, 2);
		this.tableRes = table;
		table.getTableHeader().getColumnModel().getColumn(0).setHeaderValue("Residue");
		table.getTableHeader().getColumnModel().getColumn(1).setHeaderValue("Residue Mutation");

		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		table.setSize(300, 150);
		table.setMinimumSize(new Dimension(300, 150));
		table.setShowVerticalLines(true);
		scrollPane.setSize(300, 150);
		scrollPane.setMinimumSize(new Dimension(300, 150));
		new ExcelAdapter(table); // allow copy/paste
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 2;
		gridConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		gridConstraints.insets = new Insets(0, 10, 0, 0);
		gridConstraints.gridheight = 5;
		panel.add(scrollPane, gridConstraints);

		// adding Run Dynamine button
		JButton mutate = new JButton("Run DynaMine");
		mutate.addActionListener(new DynaMineResultsListener(this, "run", registrar, panel, table));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 2;
		gridConstraints.anchor = GridBagConstraints.PAGE_END;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		panel.add(mutate, gridConstraints);

		// adding Download last analysis button
		JButton download = new JButton("Download Last Prediction Results");
		download.addActionListener(new DynaMineResultsListener(this, "download", registrar, panel, table));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 3;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		gridConstraints.anchor = GridBagConstraints.PAGE_START;
		panel.add(download, gridConstraints);

		// adding Download all analysis button
		JButton downloadAll = new JButton("Download All Prediction Results");
		downloadAll.addActionListener(new DynaMineResultsListener(this, "downloadAll", registrar, panel, table));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 4;
		gridConstraints.anchor = GridBagConstraints.PAGE_START;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		panel.add(downloadAll, gridConstraints);

		// adding loading tool
		JLabel loading = new JLabel();
		this.loading = loading;
		loading.setMinimumSize(new Dimension(32, 32));
		loading.setOpaque(false);
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 5;
		gridConstraints.anchor = GridBagConstraints.PAGE_START;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		panel.add(loading, gridConstraints);

		// adding clear graph button
		JButton clear = new JButton("Clear Graph");
		clear.addActionListener(new DynaMineResultsListener(this, "clear", registrar, panel, table));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 6;
		gridConstraints.anchor = GridBagConstraints.PAGE_END;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		panel.add(clear, gridConstraints);

		// adding close Panel button
		JButton close = new JButton("Close Panel");
		close.addActionListener(new DynaMineResultsListener(this, "close", registrar, panel, null));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 7;
		gridConstraints.anchor = GridBagConstraints.PAGE_START;
		gridConstraints.insets = new Insets(0, 0, 0, 10);
		panel.add(close, gridConstraints);

		// adding clear table button
		JButton clearTable = new JButton("Clear Table");
		clearTable.addActionListener(new DynaMineResultsListener(this, "clearTable", registrar, panel, table));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 7;
		gridConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		gridConstraints.insets = new Insets(0, 10, 0, 0);
		panel.add(clearTable, gridConstraints);

		// adding instruction label
		JLabel instructions = new JLabel(
				"<html><body><i /><b>Mutations</b><br>Select residues in network<br>Type mutated amino acid in 1-letter format<br>e.g.: F221 -> A for mutation into alanine</body></html>");
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		Border paddingBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		instructions.setBorder(BorderFactory.createCompoundBorder(border, paddingBorder));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 8;
		gridConstraints.gridheight = 2;
		gridConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridConstraints.insets = new Insets(10, 10, 10, 10);
		panel.add(instructions, gridConstraints);

		// adding DynaMine explanations
		JLabel explanation = new JLabel(
				"<html><body><i /><b>S² interpretation</b><br>0 - 0.69 : Flexible <br>0.69 - 0.8 : Context Dependent <br> > 0.8  : Rigid</body></html>");
		border = BorderFactory.createLineBorder(Color.BLACK);
		paddingBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		explanation.setBorder(BorderFactory.createCompoundBorder(border, paddingBorder));
		gridConstraints = new GridBagConstraints();
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 8;
		gridConstraints.gridheight = 2;
		gridConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridConstraints.insets = new Insets(10, 10, 10, 10);
		panel.add(explanation, gridConstraints);
	}

	/**
	 * Perform DynaMine prediction with sequence mutations
	 * 
	 * @param table
	 *            table containing all mutations
	 * @throws InterruptedException
	 */
	public void analysisWithMutation(JTable table) throws InterruptedException {
		// ******************************************************************************
		// Creation
		// of sequence with
		// mutations
		// **********************************************************
		ArrayList<String[]> residueToMutate = new ArrayList<String[]>();

		this.seqName = "Original";

		// retrieving all mutations
		for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++) {
			if (table.getValueAt(rowIndex, 1).toString().length() > 1) {
				JOptionPane.showMessageDialog(null,
						"Mutated residue should be 1-letter formated.\nValidate with \"enter\".");
				return;
			}
			if (table.getValueAt(rowIndex, 1).toString().length() < 1)
				continue;

			String[] couple = new String[2];
			String val = table.getValueAt(rowIndex, 0).toString().toUpperCase();
			String val2 = (table.getValueAt(rowIndex, 1).toString() + val.substring(1, val.length())).toUpperCase();

			if (val != null && val.trim().length() > 0 && val2 != null && val2.trim().length() > 0) // just
																									// check
																									// the
																									// first
																									// val
																									// in
																									// JTable.
			{
				couple[0] = val;
				couple[1] = val2;
				residueToMutate.add(couple);
				// set the name of the sequence
				if (this.seqName.equals("Original"))
					this.seqName = val2;
				else
					this.seqName += val2;
			} else
				break;
		}

		// creating sequence with mutations
		String newSequence = this.sequence;

		for (int i = 0; i < residueToMutate.size(); i++) {
			int pos = 0;
			while (this.resIndexSeq[pos] != Integer.parseInt(residueToMutate.get(i)[0].substring(1))) {
				pos++;
			}
			if (this.sequence.charAt(pos) == residueToMutate.get(i)[0].charAt(0))
				newSequence = RINUtils.mutateSequence(newSequence, pos, residueToMutate.get(i)[1].charAt(0));
			else {
				JOptionPane.showMessageDialog(null,
						"There is no " + residueToMutate.get(i)[0].charAt(0) + " residu at position "
								+ Integer.parseInt(residueToMutate.get(i)[1].substring(1))
								+ "in the original sequence.");
				return;
			}
		}
		System.out.println("New sequence:\n" + newSequence);

		loading.setIcon(new ImageIcon(this.getClass().getClassLoader().getResource("ajax-loader.gif")));

		CyNetwork net = this.appmanag.getCurrentNetwork();
		ArrayList<ArrayList<String>> values = runAnalysis(newSequence,
				net.getRow(net).get(CyNetwork.NAME, String.class).replace(" ", "_") + "_Mutated_Sequence_" + seqName);

		this.nbseq++;

		// *********************************************************************
		// Creation
		// of the new
		// plot
		// *********************************************************************
		XYPlot categoryplot = lineChart.getXYPlot();

		XYSeriesCollection data2 = new XYSeriesCollection();
		XYSeries serie2 = new XYSeries(this.seqName);
		for (int i = 0; i < values.size(); i++) {
			ArrayList<String> couple = values.get(i);
			serie2.add(i, Double.parseDouble(couple.get(1)));
		}
		data2.addSeries(serie2);

		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

		Random rdm = new Random();// permits to choose a random color then
		renderer.setSeriesPaint(0, Color.getColor(this.color[rdm.nextInt(this.color.length)]));
		renderer.setSeriesStroke(0, new BasicStroke(1.0f));
		renderer.setSeriesShapesVisible(0, false);

		categoryplot.setDataset(1 + this.nbseq, data2);
		categoryplot.setRenderer(1 + this.nbseq, renderer);
		loading.setIcon(null);
	}

	/**
	 * Select node in network
	 * 
	 * @param pos
	 *            position in sequence of residue
	 */
	public void selectNode(int pos) {
		CyNetwork network = this.appmanag.getCurrentNetwork();
		// check if the DynaMine graph corresponds to the displayed network
		if ((network.getRow(network).get(CyNetwork.NAME, String.class)).equals(this.lineChartNetworkName)) {
			for (CyNode node : network.getNodeList()) {
				CyRow row = network.getRow(node);
				if (row.get("ResChain", String.class).equals(this.chain)) {
					if (row.get("ResIndex", Integer.class) == this.resIndexSeq[pos]) {
						// select if node selected, unselect if not
						if (row.get(CyNetwork.SELECTED, Boolean.class).equals(Boolean.FALSE))
							row.set(CyNetwork.SELECTED, true);
						else
							row.set(CyNetwork.SELECTED, false);
					}
				}
			}
		} else {
			JOptionPane.showMessageDialog(null,
					"The selected DynaMine graph doesn't correspond to the displayed network.\n");
		}
	}

	/**
	 * Creation of the result Panel
	 * 
	 * @param myNetwork
	 *            CyNetwork to display in Result Panel
	 * @param values
	 *            S values from the CyNetwork
	 */
	public void createResultPanel(CyNetwork myNetwork, ArrayList<ArrayList<String>> values) {
		// *****************************************************************************************
		// Creation
		// of the DynaMine
		// plot
		// **********************************************************
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries serie1 = new XYSeries(seqName);
		for (int i = 0; i < values.size(); i++) {
			ArrayList<String> couple = values.get(i);
			serie1.add(i, Double.parseDouble(couple.get(1)));
		}
		dataset.addSeries(serie1);

		// get all label for X axis
		String[] xvalues = new String[values.size()];
		for (int i = 0; i < values.size(); i++) {
			ArrayList<String> couple = values.get(i);
			xvalues[i] = couple.get(0) + this.resIndexSeq[i];
		}

		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesPaint(0, Color.GREEN);
		renderer.setSeriesStroke(0, new BasicStroke(1.0f));
		renderer.setSeriesShapesVisible(0, false);

		ValueAxis xAxis = new SymbolAxis("Sequence", xvalues);
		xAxis.setVerticalTickLabels(false);
		ValueAxis yAxis = new NumberAxis("S²");
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
		plot.getDomainAxis().setVerticalTickLabels(true);
		plot.setDomainGridlinesVisible(false);
		plot.setRangeGridlinesVisible(false);
		plot.setBackgroundPaint(Color.WHITE);

		NumberAxis domain = (NumberAxis) plot.getDomainAxis();
		domain.setRange(0.00, 1.2);
		domain.setTickUnit(new NumberTickUnit(5));
		domain.setVerticalTickLabels(true);
		domain.setAutoRange(true);

		// specify the name of the network and the associated chain
		this.lineChart = new JFreeChart("S² prediction on " + lineChartNetworkName + "\nChain " + this.chain,
				new Font("Arial", 0, 18), plot, true);

		ChartPanel chartPanel = new ChartPanel(lineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
		chartPanel.setSize(new java.awt.Dimension(560, 367));
		chartPanel.setMinimumSize(new java.awt.Dimension(560, 367));

		// ******************************************************************
		// Creation of interval marker (Rigid, Flexible, Context
		// Dependent)
		// ******************************************************************
		IntervalMarker marker = new IntervalMarker(0.69, 0.8);
		marker.setAlpha(0.3f);
		marker.setLabelAnchor(RectangleAnchor.RIGHT);
		lineChart.getXYPlot().addRangeMarker(marker);

		// **********************************************************
		// Creation of
		// result
		// Panel
		// **********************************************************

		MyCytoPanel myPanel = new MyCytoPanel();

		registrar.registerService(myPanel, CytoPanelComponent.class, new Properties());

		CytoPanel CytoresultPanel = this.cytoscapeDesktopService.getCytoPanel(CytoPanelName.EAST);
		if (CytoresultPanel.getState() == CytoPanelState.HIDE) {
			CytoresultPanel.setState(CytoPanelState.DOCK);
		}
		// ******************************************************************************************************************************************************************

		myPanel.setLayout(gridlayout);
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 0;
		gridConstraints.gridwidth = 2;
		gridConstraints.weightx = 1;
		gridConstraints.weighty = 1;
		gridConstraints.fill = GridBagConstraints.BOTH;
		gridConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		myPanel.add(chartPanel, gridConstraints);

		// set listener to get selection of node
		registrar.registerService(this, RowsSetListener.class, new Properties());
		addResultPanelComponent(myPanel, myNetwork.getNodeCount());

		plotRender = chartPanel.getChartRenderingInfo().getPlotInfo();

		// set listener to get plot point selection
		chartPanel.addChartMouseListener(new ChartMouseListener() {

			@Override
			public void chartMouseMoved(ChartMouseEvent event) {
			}

			@Override
			public void chartMouseClicked(ChartMouseEvent event) {
				final ChartMouseEvent eventLocal = event;
				if (event.getEntity().toString().startsWith("XYItemEntity")) {
					XYItemEntity entity = (XYItemEntity) event.getEntity();
					int xValue = entity.getItem();
					RINUtils.clearTable(tableRes);
					selectNode(xValue);
				} else {
					// get the X value and select the corresponding point
					ChartPanel hostChartPanel = (ChartPanel) event.getTrigger().getComponent();

					// repaints and wait for the update before getting X index
					// with getDomainCrosshairValue()
					hostChartPanel.repaint();

					java.awt.EventQueue.invokeLater(new Runnable() {
						@Override
						public void run() {
							JFreeChart chart = eventLocal.getChart();
							XYPlot plot = chart.getXYPlot();
							selectNode((int) Math.round(plot.getDomainCrosshairValue()));
						}
					});
				}
			}
		});

		// set the right result panel
		int index = CytoresultPanel.indexOfComponent(myPanel);
		if (index == -1) {
			return;
		}
		CytoresultPanel.setSelectedIndex(index);

	}

	/**
	 * Clear all graph in DynaMine graph, except Original sequence graph
	 */
	public void clearGraph() {

		XYPlot categoryplot = lineChart.getXYPlot();
		for (int i = categoryplot.getDatasetCount() - 1; i > 1; i--) {
			categoryplot.setDataset(i, null);
		}
		this.nbseq = 0;
		this.urlResults = new ArrayList<String>();
	}

	/**
	 * Save all results from DynaMine prediction
	 */
	public void saveAllresults() {
		File path = new File(".");
		JFileChooser dialogue = new JFileChooser(path);
		dialogue.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		if (dialogue.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			path = dialogue.getSelectedFile();
		}

		if (this.urlResults.size() == 0) {
			JOptionPane.showMessageDialog(null, "No analysis to download");
		}
		for (String result : this.urlResults) {
			RINUtils.saveFile(result, true, path);
		}
	}

	/**
	 * Run Dynamine prediction
	 * 
	 * @param sequence
	 *            sequence to submit
	 * @param sequenceName
	 *            sequence name
	 * @return array with residues associated with their s value
	 * @throws InterruptedException
	 */
	public ArrayList<ArrayList<String>> runAnalysis(String sequence, String sequenceName) throws InterruptedException {
		// parameters
		String predictionOnly = "true";
		String protocol = "1.0";
		String urlParam = "batch=%7B%22predictions_only%22%3A+" + predictionOnly + "%2C+%22json_api_key%22%3A+%22"
				+ this.json_key + "%22%2C+%22protocol%22%3A+%22" + protocol + "%22%2C+%22sequences%22%3A+%7B%22"
				+ sequenceName + "%22%3A+%22" + sequence + "%22%7D%7D";

		String response = submitJob(urlParam);

		// save url of the zip result file
		this.lastUrlResults = extractResults(response);
		this.urlResults.add(this.lastUrlResults);

		ArrayList<ArrayList<String>> values = sValues(response, sequence.length());

		return values;
	}

	/**
	 * Main function of DynaMineAnalysis
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {

		this.tm = arg0;

		tm.setTitle("Run DynaMine prediction...");

		// Get all of our services

		this.chain = chainToAnalyze.getSelectedValue();

		CyNetwork myNetwork = this.appmanag.getCurrentNetwork();
		String sequence;

		this.lineChartNetworkName = myNetwork.getRow(myNetwork).get(CyNetwork.NAME, String.class);

		sequence = networkSequence(myNetwork);
		System.out.println("Sequence:\n" + sequence);
		this.sequence = sequence;

		// Run dynamine prediction
		ArrayList<ArrayList<String>> values = runAnalysis(sequence,
				myNetwork.getRow(myNetwork).get(CyNetwork.NAME, String.class).replace(" ", "_"));
		addSColumn(myNetwork, values);
		createDynaMineStyle(values, DynaMineS2StyleName);
		createResultPanel(myNetwork, values);

		RINUtils.unselectAllNodesAndEdges(myNetwork);
	}

	/**
	 * Catch node selection in Cytoscape view
	 */
	@Override
	public void handleEvent(RowsSetEvent e) {
		if (e.containsColumn("selected")
				&& e.getSource().getClass().toString().equals("class org.cytoscape.model.internal.CyTableImpl")) {
			CyNetwork network = this.appmanag.getCurrentNetwork();

			// check if the DynaMine graph corresponds to the displayed network
			// to continue
			if (!(network.getRow(network).get(CyNetwork.NAME, String.class)).equals(this.lineChartNetworkName)) {
				return;
			}

			List<CyNode> nodes = CyTableUtil.getNodesInState(network, "selected", true);

			double[] sequenceTab = new double[this.resIndexSeq[this.resIndexSeq.length - 1]];
			// list of selected residues, formated as one-letter amino acid
			// (e.g.: A102)
			selectedResidues = new ArrayList<String>();
			for (CyNode node : nodes) {
				CyRow row = network.getRow(node);
				if (row.get("ResChain", String.class).equals(this.chain)) {
					Double res = row.get("S²", Double.class);
					String type = (row.get("ResType", String.class)).toUpperCase();
					int index = row.get("ResIndex", Integer.class);

					sequenceTab[index - 1] = res;

					// retrieve the one letter code for amino acid; if unknown,
					// set X
					if (aa.containsKey(type))
						selectedResidues.add(aa.get(type) + Integer.toString(index));
					else
						selectedResidues.add('X' + Integer.toString(index));
				}
			}

			this.sortSelectedResidues();

			// ********************************************************
			// Creating
			// bar graph in result
			// panel
			// ********************************************************
			XYSeriesCollection data2 = new XYSeriesCollection();
			XYSeries serie2 = new XYSeries("Selected amino acids");

			for (int i = 0; i < this.resIndexSeq.length; i++)
				serie2.add(i, sequenceTab[this.resIndexSeq[i] - 1]);

			data2.addSeries(serie2);
			XYPlot categoryplot = lineChart.getXYPlot();
			XYBarRenderer bargraph = new XYBarRenderer();
			bargraph.setShadowVisible(false);
			bargraph.setSeriesPaint(0, Color.RED);
			XYBarRenderer.setDefaultBarPainter(new StandardXYBarPainter());
			categoryplot.setDataset(1, data2);
			categoryplot.setRenderer(1, bargraph);

			// *****************************************************
			// * Adding residue to residues list in result panel
			// *****************************************************

			setResiduesToTable();
		}
	}

	/**
	 * Sorts selected residues by index
	 */

	public void sortSelectedResidues() {

		try {
			Collections.sort(selectedResidues, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					Integer o1Integer = Integer.parseInt(o1.substring(1, o1.length()));
					Integer o2Integer = Integer.parseInt(o2.substring(1, o2.length()));
					return o1Integer.intValue() - o2Integer.intValue();
				}
			});
		} catch (Exception e) {
			System.out.println("Impossible to sort DynaMine table");
		}
	}

	/**
	 * Adds residues to table in the results panel
	 */

	private void setResiduesToTable() {
		try {
			RINUtils.clearTable(this.tableRes);
		} catch (NullPointerException npe) {
			return;
		}

		int y = 0;

		// a residue is not added twice to the result panel
		for (int i = 0; i < selectedResidues.size(); i++) {
			y = 0;
			while (this.tableRes.getValueAt(y, 0) != null
					&& (this.tableRes.getValueAt(y, 0).toString()).trim().length() > 0) {
				if (selectedResidues.get(i).equals(this.tableRes.getValueAt(y, 0)))
					selectedResidues.set(i, "");
				y++;
			}
		}

		// then add values
		for (int i = 0; i < selectedResidues.size(); i++) {
			if (!selectedResidues.get(i).equals("")) {
				this.tableRes.setValueAt(selectedResidues.get(i), y, 0);
				y++;
			}
		}
	}

	/**
	 * Listener of all buttons in Result Panel
	 * 
	 */
	class DynaMineResultsListener implements ActionListener {

		private final CyServiceRegistrar registrar;
		private final MyCytoPanel panel;
		private final String name;
		private final JTable table;
		private final DynaMineAnalysis parent;

		public DynaMineResultsListener(DynaMineAnalysis parent, String name, CyServiceRegistrar registrar,
				MyCytoPanel panel, JTable table) {
			this.registrar = registrar;
			this.panel = panel;
			this.name = name;
			this.table = table;
			this.parent = parent;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (name.equals("close"))
				registrar.unregisterService(panel, CytoPanelComponent.class);
			else if (name.equals("run")) {
				Thread t = new Thread() {
					@Override
					public void run() {
						try {
							parent.analysisWithMutation(table);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				};
				t.start();
			} else if (name.equals("clear")) {
				parent.clearGraph();
			} else if (name.equals("clearTable")) {
				for (int i = 0; i < table.getRowCount(); i++)
					for (int j = 1; j < table.getColumnCount(); j++) {
						table.setValueAt("", i, j);
					}
			} else if (name.equals("download")) {
				RINUtils.saveFile(lastUrlResults, false, null);
			} else if (name.equals("downloadAll")) {
				saveAllresults();
			}
		}
	}

	//to retrieve results of DynaMine calculation in JSON format
	
	@SuppressWarnings("unchecked")
	@Override
	public <R> R getResults(Class<? extends R> type) {
		if (type.equals(String.class)) {
			return (R) RINUtils.getJson(S2Score);
		} 
		else if (type.equals(JSONResult.class)) {
			JSONResult res = () -> {return RINUtils.getJson(S2Score);};
			return (R)(res);
		} else {
			return (R) null;
		}
	}
	
	@Override
	public List<Class<?>> getResultClasses() {
		return Arrays.asList(String.class, JSONResult.class);
	}
}
