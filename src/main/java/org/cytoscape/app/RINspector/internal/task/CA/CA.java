package org.cytoscape.app.RINspector.internal.task.CA;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cytoscape.app.RINspector.internal.task.CloneNetworkTask;
import org.cytoscape.app.RINspector.internal.task.tools.CyNetworkUtils;
import org.cytoscape.app.RINspector.internal.task.tools.MathUtils;
import org.cytoscape.app.RINspector.internal.task.tools.RINUtils;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.group.CyGroupFactory;
import org.cytoscape.group.CyGroupManager;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.PassthroughMapping;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.json.JSONResult;
import org.cytoscape.work.util.ListSingleSelection;

/**
 * Class that permits to perform centrality analyses
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */


public class CA extends AbstractTask implements ObservableTask{

	// global Cytoscape Variables and tools
	private final CyApplicationManager appmanag;
	private final CyNetworkManager netmgr;
	private final CyNetworkViewManager networkViewManager;
	private final VisualMappingManager vmm;
	private final CyNetworkFactory netFactory;
	private final CyNetworkViewFactory netViewFactory;
	private final CyNetworkNaming naming;
	private final CyNetworkTableManager netTableMgr;
	private final CyRootNetworkManager rootNetMgr;
	private final CyGroupManager groupMgr;
	private final CyGroupFactory groupFactory;
	private final RenderingEngineManager renderingEngineMgr;
	private final VisualMappingFunctionFactory visMapFact;
	private final VisualStyleFactory visFactFactory;
	private final VisualMappingFunctionFactory vmfFactoryP;
	private TaskMonitor tm;

	/**
	 * 
	 * Flag indicating if node(edge) betweenness and stress should be computed.
	 * It is set to false if the
	 * 
	 * number of shortest paths exceeds the maximum long value.
	 * 
	 */

	private boolean computeNB;

	/**
	 * 
	 * Map of all nodes with their respective node betweenness information,
	 * which stores information
	 * 
	 * needed for the node betweenness calculation.
	 * 
	 */

	private Map<CyNode, NodeBetweenInfo> nodeBetweenness;

	/**
	 * 
	 * Map of all nodes with their respective edge betweenness.
	 * 
	 */

	private Map<CyEdge, Double> edgeBetweenness;

	/**
	 * 
	 * Map of all nodes with their respective stress, i.e. number of shortest
	 * paths passing through
	 * 
	 * a node.
	 * 
	 */

	private Map<CyNode, Long> stress;

	// variables used to compute avspl values
	private Set<CyNode> visited = null;
	private long sPathLengths[];

	////// Style //////

	// for node fill color
	private final BoundaryRangeValues<Paint> brv1 = new BoundaryRangeValues<Paint>(Color.WHITE, Color.YELLOW,
			Color.YELLOW);
	private final BoundaryRangeValues<Paint> brv2 = new BoundaryRangeValues<Paint>(Color.ORANGE, Color.ORANGE,
			Color.ORANGE);
	private final BoundaryRangeValues<Paint> brv3 = new BoundaryRangeValues<Paint>(Color.RED, Color.RED, Color.RED);
	private final double CAMinRangeValueForRelevantZScores = 2d;
	private final double CAMiddleRangeValueForRelevantZScores = 3d;
	private final double CAMaxRangeValueForRelevantZScores = 4d;

	// for node size
	private final BoundaryRangeValues<Double> nodeHeight1 = new BoundaryRangeValues<Double>(20d, 20d, 20d);
	private final BoundaryRangeValues<Double> nodeHeight2 = new BoundaryRangeValues<Double>(50d, 50d, 50d);

	private final BoundaryRangeValues<Double> nodeWidth1 = new BoundaryRangeValues<Double>(40d, 40d, 40d);
	private final BoundaryRangeValues<Double> nodeWidth2 = new BoundaryRangeValues<Double>(100d, 100d, 100d);

	// for node label size
	private final BoundaryRangeValues<Integer> nodeLabelSizeCA1 = new BoundaryRangeValues<Integer>(20, 20, 20);
	private final BoundaryRangeValues<Integer> nodeLabelSizeCA2 = new BoundaryRangeValues<Integer>(50, 50, 50);
	private final BoundaryRangeValues<Integer> nodeLabelSizeCA3 = new BoundaryRangeValues<Integer>(100, 100, 100);

	private final String zScoreRCAColumnName = "Z-score_RCA";
	private final String zScoreBCAColumnName = "Z-score_BCA";
	private final String zScoreCCAColumnName = "Z-score_CCA";

	//associates SUID with Z-Scores
	private HashMap<Long, Double> ZScoreRCA;
	private HashMap<Long, Double> ZScoreBCA;
	private HashMap<Long, Double> ZScoreCCA;
	
	private final int edgeTransparency = 150;

	enum TypeOfCentralityAnalysis {
		RCA("Residue (ASPL change under removal of individual nodes) - RCA"), BCA("Betweenness - BCA"), CCA(
				"Closeness - CCA");

		String type;

		TypeOfCentralityAnalysis(String aType) {
			this.type = aType;
		}

		public String getTypeOfCentralityAnalysis() {
			return type;
		}
		
		public void setTypeOfCentralityAnalysis(String centralityType) {
		       this.type = centralityType;
		  }

		public static String[] getTypesOfCentralityAnalysis() {
			String[] types = new String[TypeOfCentralityAnalysis.values().length];

			TypeOfCentralityAnalysis[] allValues = TypeOfCentralityAnalysis.values();

			for (int i = 0; i < allValues.length; i++) {
				types[i] = allValues[i].getTypeOfCentralityAnalysis();
			}
			return types;
		}
	}

	@Tunable(description = "Select the type of centrality analysis ", groups = { "Analysis Type" }, longDescription="Type of centrality analysis (among: \"Residue (ASPL change under removal of individual nodes) - RCA\", \"Betweenness - BCA\" and \"Closeness - CCA\") ", exampleStringValue="Residue (ASPL change under removal of individual nodes) - RCA", context=Tunable.BOTH_CONTEXT)
	public ListSingleSelection<String> analysisType = new ListSingleSelection<String>(
			TypeOfCentralityAnalysis.getTypesOfCentralityAnalysis());

	public CA(final CyApplicationManager cyApplicationManager, final CyNetworkManager netmgr,
			final CyNetworkViewManager networkViewManager, final VisualMappingManager vmm,
			final CyNetworkFactory netFactory, final CyNetworkViewFactory netViewFactory, final CyNetworkNaming naming,
			final CyNetworkTableManager netTableMgr, final CyRootNetworkManager rootNetMgr,
			final CyGroupManager groupMgr, final CyGroupFactory groupFactory,
			final RenderingEngineManager renderingEngineMgr, final CyNetworkViewFactory nullNetworkViewFactory,
			final VisualMappingFunctionFactory visMapFact, final VisualStyleFactory visFactFactory,
			final VisualMappingFunctionFactory vmfFactoryP) {
		this.appmanag = cyApplicationManager;
		this.netmgr = netmgr;
		this.networkViewManager = networkViewManager;
		this.vmm = vmm;
		this.netFactory = netFactory;
		this.netViewFactory = netViewFactory;
		this.naming = naming;
		this.netTableMgr = netTableMgr;
		this.rootNetMgr = rootNetMgr;
		this.groupMgr = groupMgr;
		this.groupFactory = groupFactory;
		this.renderingEngineMgr = renderingEngineMgr;
		this.visFactFactory = visFactFactory;
		this.visMapFact = visMapFact;
		this.vmfFactoryP = vmfFactoryP;
	}

	/**********************************************************************************************************************************************************
	 * avSpl values computing functions
	 **********************************************************************************************************************************************************/
	/**
	 * Compute the average shortest path length
	 * 
	 * Retrieved and adapted from NetworkAnalyzer (Yassen Assenov, Sven-Eric
	 * Schelhorn, Nadezhda Doncheva)
	 * 
	 * @param network
	 * @return the average shortest path length
	 */

	public double avSplCalculation(CyNetwork network) {
		this.visited = new HashSet<CyNode>(network.getNodeCount());
		this.sPathLengths = new long[network.getNodeCount()];
		SimpleUndirParams params = new SimpleUndirParams();

		ArrayList<CyNode> nodes = (ArrayList<CyNode>) network.getNodeList();

		for (int i = 0; i < nodes.size(); i++) {
			PathLengthData pathLengths = computeSPandSN(nodes.get(i), network);
			int eccentricity = pathLengths.getMaxLength();
			if (params.diameter < eccentricity)
				params.diameter = eccentricity;
		}

		long connPairs = 0L;
		long totalPathLength = 0L;
		for (int i = 1; i <= params.diameter; i++) {
			connPairs += sPathLengths[i];
			totalPathLength += i * sPathLengths[i];
		}

		if (params.diameter > 0) {
			double avspl = (double) totalPathLength / (double) connPairs;
			return avspl;
		}
		return 0;
	}

	/**
	 * Compute all path lengths for one node in a network
	 * 
	 * Retrieved and adapted from NetworkAnalyzer (Yassen Assenov, Sven-Eric
	 * Schelhorn, Nadezhda Doncheva)
	 * 
	 * @param aNode
	 * @param network
	 * @return PathLengthData
	 */
	private PathLengthData computeSPandSN(CyNode aNode, CyNetwork network) {
		visited.clear();
		visited.add(aNode);
		Set<CyNode> nbs = null;
		LinkedList<CyNode> reachedNodes = new LinkedList<CyNode>();
		reachedNodes.add(aNode);
		reachedNodes.add(null);
		int currentDist = 1;
		PathLengthData result = new PathLengthData();
		label0: for (CyNode currentNode = reachedNodes.removeFirst(); !reachedNodes
				.isEmpty(); currentNode = reachedNodes.removeFirst()) {
			if (currentNode == null) {
				currentDist++;
				reachedNodes.add(null);
				continue;
			}
			Set<CyNode> neighbors = RINUtils.getNeighbors(currentNode, network);
			if (nbs == null)
				nbs = neighbors;
			Iterator<CyNode> i$ = neighbors.iterator();
			do {
				CyNode neighbor;
				do {
					if (!i$.hasNext())
						continue label0;
					neighbor = i$.next();
				} while (!visited.add(neighbor));
				// int snCount = currentDist <= 2 ? countNeighborsIn(nbs,
				// neighbor, network) : 0;
				sPathLengths[currentDist]++;
				result.addSPL(currentDist);
				reachedNodes.add(neighbor);
			} while (true);
		}

		return result;
	}

	/**
	 * 
	 * Accumulates the node and edge betweenness of all nodes in a connected
	 * component. The node betweenness is calculate using the algorithm of
	 * Brandes (U. Brandes: A Faster Algorithm for Betweenness Centrality.
	 * Journal of Mathematical Sociology 25(2):163-177, 2001). The edge
	 * betweenness is calculated as used by Newman and Girvan (M.E. Newman and
	 * M. Girvan: Finding and Evaluating Community Structure in Networks. Phys.
	 * Rev. E Stat. Nonlin. Soft. Matter Phys., 69, 026113.). In each run of
	 * this method a different source node is chosen and the betweenness of all
	 * nodes is replaced by the new one. For the final result this method has to
	 * be run for all nodes of the connected component.
	 * 
	 * 
	 * This method uses a breadth-first search through the network, starting
	 * from a specified source node, in order to find all paths to the other
	 * nodes in the network and to accumulate their betweenness.
	 * 
	 * 
	 * @param source
	 * 
	 *            CyNode where a run of breadth-first search is started, in
	 *            order to accumulate the node and edge betweenness of all other
	 *            nodes
	 * 
	 */

	private void computeNBandEB(CyNode source, CyNetwork network) {

		LinkedList<CyNode> done_nodes = new LinkedList<CyNode>();
		LinkedList<CyNode> reached = new LinkedList<CyNode>();
		HashMap<CyEdge, Double> edgeDependency = new HashMap<CyEdge, Double>();
		HashMap<CyNode, Long> stressDependency = new HashMap<CyNode, Long>();

		final NodeBetweenInfo sourceNBInfo = nodeBetweenness.get(source);
		sourceNBInfo.setSource();
		reached.add(source);
		stressDependency.put(source, Long.valueOf(0));

		// Use BFS to find shortest paths from source to all nodes in the
		// network

		while (!reached.isEmpty()) {

			final CyNode current = reached.removeFirst();

			done_nodes.addFirst(current);

			final NodeBetweenInfo currentNBInfo = nodeBetweenness.get(current);

			final Set<CyNode> neighbors = getNeighbors(network, current);

			for (CyNode neighbor : neighbors) {

				final NodeBetweenInfo neighborNBInfo = nodeBetweenness.get(neighbor);

				final List<CyEdge> edges = network.getConnectingEdgeList(current, neighbor, CyEdge.Type.ANY);

				final int expectSPLength = currentNBInfo.getSPLength() + 1;

				if (neighborNBInfo.getSPLength() < 0) {

					// Neighbor traversed for the first time

					reached.add(neighbor);

					neighborNBInfo.setSPLength(expectSPLength);

					stressDependency.put(neighbor, Long.valueOf(0));

				}

				// shortest path via current to neighbor found

				if (neighborNBInfo.getSPLength() == expectSPLength) {

					neighborNBInfo.addSPCount(currentNBInfo.getSPCount());

					// check for long overflow

					if (neighborNBInfo.getSPCount() < 0) {

						computeNB = false;

					}

					// add predecessors and outgoing edges, needed for

					// accumulation of betweenness scores

					neighborNBInfo.addPredecessor(current);

					for (final CyEdge edge : edges) {

						currentNBInfo.addOutedge(edge);

					}

				}

				// initialize edge dependency

				for (final CyEdge edge : edges) {

					edgeDependency.put(edge, new Double(0.0));

				}

			}

		}

		// Return nodes in order of non-increasing distance from source

		while (!done_nodes.isEmpty()) {

			final CyNode current = done_nodes.removeFirst();

			final NodeBetweenInfo currentNBInfo = nodeBetweenness.get(current);

			if (currentNBInfo != null) {

				final long currentStress = stressDependency.get(current).longValue();

				while (!currentNBInfo.isEmptyPredecessors()) {

					final CyNode predecessor = currentNBInfo.pullPredecessor();

					final NodeBetweenInfo predecessorNBInfo = nodeBetweenness.get(predecessor);

					predecessorNBInfo.addDependency((1.0 + currentNBInfo.getDependency())

							* ((double) predecessorNBInfo.getSPCount() / (double) currentNBInfo

									.getSPCount()));

					// accumulate all sp count

					final long oldStress = stressDependency.get(predecessor).longValue();

					stressDependency.put(predecessor, new Long(oldStress + 1 + currentStress));

					// accumulate edge betweenness

					final List<CyEdge> edges = network.getConnectingEdgeList(predecessor, current, CyEdge.Type.ANY);

					if (edges.size() != 0) {

						final CyEdge compEdge = edges.get(0);

						final LinkedList<CyEdge> currentedges = currentNBInfo.getOutEdges();

						double oldbetweenness = 0.0;

						double newbetweenness = 0.0;

						for (final CyEdge edge : edges) {

							Double betweenness = edgeBetweenness.get(edge);

							if (betweenness != null) {

								oldbetweenness = betweenness;

								break;

							}

						}

						// if the node is a leaf node in this search tree

						if (currentedges.size() == 0) {

							newbetweenness = (double) predecessorNBInfo.getSPCount()

									/ (double) currentNBInfo.getSPCount();

						} else {

							double neighbourbetw = 0.0;

							for (CyEdge neighbouredge : currentedges) {

								if (!edges.contains(neighbouredge)) {

									neighbourbetw += edgeDependency.get(neighbouredge)

											.doubleValue();

								}

							}

							newbetweenness = (1 + neighbourbetw)

									* ((double) predecessorNBInfo.getSPCount() / (double) currentNBInfo

											.getSPCount());

						}

						edgeDependency.put(compEdge, new Double(newbetweenness));

						for (final CyEdge edge : edges) {

							edgeBetweenness.put(edge, new Double(newbetweenness + oldbetweenness));

						}

					}

				}

				// accumulate node betweenness in each run

				if (!current.equals(source)) {

					currentNBInfo.addBetweenness(currentNBInfo.getDependency());

					// accumulate number of shortest paths

					final long allSpPaths = stress.get(current).longValue();

					stress.put(current, new Long(allSpPaths + currentNBInfo.getSPCount()

							* currentStress));

				}

			}

		}

	}

	/**
	 * 
	 * Gets all the neighbors of the given node.
	 * 
	 * 
	 * 
	 * @param aNode
	 * 
	 *            CyNode , whose neighbors are to be found.
	 * 
	 * @return <code>Set</code> of <code>Node</code> instances, containing all
	 *         the neighbors of
	 * 
	 *         <code>aNode</code>; empty set if the node specified is an
	 *         isolated vertex.
	 * 
	 * @see CyNetworkUtils#getNeighbors(CyNetwork, CyNode , int[])
	 * 
	 */

	private Set<CyNode> getNeighbors(CyNetwork network, CyNode aNode) {

		return CyNetworkUtils.getNeighbors(network, aNode, getIncidentEdges(network, aNode));

	}

	/**
	 * Compute the zScore value for each node
	 * 
	 * @param network
	 *            the network which contain all nodes
	 * @param values
	 *            values for which to calculate a z-score with associated SUID
	 *            of nodes
	 * @param average
	 *            average value
	 * @param standardDeviation
	 *            standard deviation
	 * @return HashMap with the association of zscore value with the nodes SUID
	 */
	public HashMap<Long, Double> zScoreForEachNode(CyNetwork network, HashMap<Long, Double> values, double averageValue,
			double standardDeviation) {
		tm.setTitle("Computing Z-score values...");
		HashMap<Long, Double> zScore = new HashMap<Long, Double>(values.size());

		Iterator<Long> keySetIterator = values.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Long key = keySetIterator.next();

			double tempZscore = MathUtils.zScore(values.get(key), averageValue, standardDeviation);
			zScore.put(key, tempZscore);
		}
		return zScore;
	}

	/**
	 * Compute all closeness values for each node in a network
	 * 
	 * Retrieved and adapted from NetworkAnalyzer (Yassen Assenov, Sven-Eric
	 * Schelhorn, Nadezhda Doncheva)
	 * 
	 */

	public HashMap<Long, Double> closenessCalculation(CyNetwork network) {
		// closeness centrality
		HashMap<Long, Double> closenessCent = new HashMap<Long, Double>(network.getNodeCount());

		this.visited = new HashSet<CyNode>(network.getNodeCount());
		this.sPathLengths = new long[network.getNodeCount()];
		SimpleUndirParams params = new SimpleUndirParams();

		// Compute number of connected components
		ConnComponentAnalyzer cca = new ConnComponentAnalyzer(network);
		Set<CCInfo> components = cca.findComponents();
		params.connectedComponentCount = components.size();

		for (CCInfo aCompInfo : components) {
			// Get nodes of connected component
			Set<CyNode> connNodes = cca.getNodesOf(aCompInfo);

			for (final CyNode node : connNodes) {

				PathLengthData pathLengths = computeSPandSN(node, network);

				final int eccentricity = pathLengths.getMaxLength();

				if (params.diameter < eccentricity) {
					params.diameter = eccentricity;
				}

				if (0 < eccentricity && eccentricity < params.radius) {
					params.radius = eccentricity;
				}

				final double apl = (pathLengths.getCount() > 0) ? pathLengths.getAverageLength() : 0;

				final double closeness = (apl > 0.0) ? 1 / apl : 0.0;
				closenessCent.put(node.getSUID(), closeness);
			}
		}

		return closenessCent;
	}

	/**
	 * Compute all betweenness values for each node in a network
	 * 
	 * Retrieved and adapted from NetworkAnalyzer (Yassen Assenov, Sven-Eric
	 * Schelhorn, Nadezhda Doncheva)
	 * 
	 */

	public HashMap<Long, Double> betweennessCalculation(CyNetwork network) {
		// betweenness centrality
		HashMap<Long, Double> betweennessCent = new HashMap<Long, Double>(network.getNodeCount());
		nodeBetweenness = new HashMap<CyNode, NodeBetweenInfo>();
		edgeBetweenness = new HashMap<CyEdge, Double>();
		stress = new HashMap<CyNode, Long>();
		computeNB = true;

		this.visited = new HashSet<CyNode>(network.getNodeCount());
		this.sPathLengths = new long[network.getNodeCount()];
		SimpleUndirParams params = new SimpleUndirParams();

		// Compute number of connected components
		ConnComponentAnalyzer cca = new ConnComponentAnalyzer(network);
		Set<CCInfo> components = cca.findComponents();
		params.connectedComponentCount = components.size();

		for (CCInfo aCompInfo : components) {
			// Get nodes of connected component
			Set<CyNode> connNodes = cca.getNodesOf(aCompInfo);

			// Initialize the parameters for node and edge betweenness
			// calculation
			nodeBetweenness.clear();
			edgeBetweenness.clear();
			stress.clear();

			for (CyNode n : connNodes) {
				nodeBetweenness.put(n, new NodeBetweenInfo(0, -1, 0.0));
				stress.put(n, Long.valueOf(0));
			}

			for (final CyNode node : connNodes) {
				computeNBandEB(node, network);
				// Reset everything except the betweenness value
				for (final CyNode n : connNodes) {

					NodeBetweenInfo nodeInfo = nodeBetweenness.get(n);

					nodeInfo.reset();

				}
			}
			// Normalize and save node betweenness

			final double nNormFactor = computeNormFactor(nodeBetweenness.size());

			for (final CyNode n : connNodes) {

				if (computeNB) {

					final NodeBetweenInfo nbi = nodeBetweenness.get(n);

					double nb = nbi.getBetweenness() * nNormFactor;

					if (Double.isNaN(nb)) {

						nb = 0.0;

					}

					betweennessCent.put(n.getSUID(), nb);
				}

			} // end iterate over nodes
		}

		return betweennessCent;
	}

	/**
	 * Compute the avspl value for each node
	 * 
	 * @param network
	 *            the network which contain all nodes
	 * @param nodes
	 *            list of nodes in the network
	 * @return hasmap of every avspl containing the avspl corresponding to the
	 *         node name
	 */
	public HashMap<Long, Double> avsplForEachNode(CyNetwork network, ArrayList<CyNode> nodes) {
		HashMap<Long, Double> avspl = new HashMap<Long, Double>(nodes.size());

		for (int i = 0; i < nodes.size(); i++) {
			CloneNetworkTask clone = new CloneNetworkTask(network, netmgr, networkViewManager, vmm, netFactory,
					netViewFactory, naming, appmanag, netTableMgr, rootNetMgr, groupMgr, groupFactory,
					renderingEngineMgr, null);
			// copy the network to remove one node and get the nodes of the
			// network
			CyNetwork networkTemp = RINUtils.getNetworkCopy(clone);

			// ArrayList of treated nodes
			ArrayList<CyNode> nodesTreated = new ArrayList<CyNode>();

			// get the correspondence between the node of the original network
			// and the node of the new network
			Map<CyNode, CyNode> orig2NewNodeMap = clone.getOrig2NewNodeMap();

			CyNode currentNode = orig2NewNodeMap.get(nodes.get(i));

			nodesTreated.add(currentNode);
			String name = networkTemp.getRow(currentNode).get(CyNetwork.NAME, String.class);

			tm.setStatusMessage("Computing avSpl value for the network without " + name + " residue.");

			// remove the node
			networkTemp.removeNodes(nodesTreated);

			// then calculate the average shortest path length
			double tempAvspl = avSplCalculation(networkTemp);

			avspl.put((nodes.get(i)).getSUID(), tempAvspl);

			// progression
			double tempI = i;
			NumberFormat Myformat = NumberFormat.getInstance();
			Myformat.setMinimumFractionDigits(MathUtils.numberOfDigits);
			Myformat.setMaximumFractionDigits(MathUtils.numberOfDigits);
			String str = Myformat.format(tempI / nodes.size());
			str = str.replace(',', '.');
			tm.setProgress(Double.parseDouble(str));

			networkTemp.dispose();
			networkTemp = null;
			clone = null;
		}
		return avspl;
	}

	/**
	 * Create RCA style according to z-score values
	 * 
	 * @param zscore
	 *            hashmap with the association of zscore value with the nodes
	 *            name
	 */
	public void createVisualStyle(HashMap<Long, CyNode> assocCyNodeSUID, HashMap<Long, Double> zscore,
			String columnName, String styleName) {
		// taskmonitor
		tm.setTitle("Adapting the current view...");

		// if style already exists, remove it to create the new one
		Iterator<VisualStyle> it = vmm.getAllVisualStyles().iterator();
		while (it.hasNext()) {
			VisualStyle curVS = it.next();
			if (curVS.getTitle().equals(styleName)) {
				vmm.setCurrentVisualStyle(curVS);
				return;
			}
		}

		// ***********************************************************************
		// Creation
		// of CA
		// style
		// ***********************************************************************

		CyNetworkView myView = this.appmanag.getCurrentNetworkView();
		VisualStyle vs = visFactFactory.createVisualStyle(vmm.getVisualStyle(myView));
		vs.setTitle(styleName);

		// ********************************************************
		// Define node
		// fill color
		// ********************************************************
		// Set node color map to attribute "Z-score"
		ContinuousMapping<Double, Paint> mappingColor = (ContinuousMapping<Double, Paint>) this.visMapFact
				.createVisualMappingFunction(columnName, Double.class, BasicVisualLexicon.NODE_FILL_COLOR);

		// Define the points
		double val1 = CAMinRangeValueForRelevantZScores;
		double val2 = CAMiddleRangeValueForRelevantZScores;
		double val3 = CAMaxRangeValueForRelevantZScores;

		// Set the points
		mappingColor.addPoint(val1, brv1);
		mappingColor.addPoint(val2, brv2);
		mappingColor.addPoint(val3, brv3);

		// add the mapping to visual style
		vs.addVisualMappingFunction(mappingColor);

		// ***********************************************************
		// Define
		// node
		// height
		// ***********************************************************
		ContinuousMapping<Double, Double> mappingHeight = (ContinuousMapping<Double, Double>) this.visMapFact
				.createVisualMappingFunction(columnName, Double.class, BasicVisualLexicon.NODE_HEIGHT);

		// Define the points
		mappingHeight.addPoint(0.0d, nodeHeight1);
		mappingHeight.addPoint(CAMaxRangeValueForRelevantZScores, nodeHeight2);

		vs.addVisualMappingFunction(mappingHeight);

		// ***********************************************************
		// Define
		// node
		// width
		// ****************************************************************
		ContinuousMapping<Double, Double> mappingWidth = (ContinuousMapping<Double, Double>) this.visMapFact
				.createVisualMappingFunction(columnName, Double.class, BasicVisualLexicon.NODE_WIDTH);

		// Define the points
		mappingWidth.addPoint(0.0d, nodeWidth1);
		mappingWidth.addPoint(CAMaxRangeValueForRelevantZScores, nodeWidth2);

		vs.addVisualMappingFunction(mappingWidth);

		// **************************************************************
		// Define
		// node
		// label
		// **************************************************************
		PassthroughMapping<String, ?> mappingLabel = (PassthroughMapping<String, ?>) this.vmfFactoryP
				.createVisualMappingFunction("Residue", String.class, BasicVisualLexicon.NODE_LABEL);

		vs.addVisualMappingFunction(mappingLabel);

		// ***************************************************************
		// Define
		// label font
		// style
		// ***************************************************************
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_FACE, new Font("Arial", Font.PLAIN, 20));

		// ***************************************************************
		// Define
		// label font
		// size
		// ***************************************************************

		ContinuousMapping<Double, Integer> mappingSizeLabel = (ContinuousMapping<Double, Integer>) this.visMapFact
				.createVisualMappingFunction(columnName, Double.class, BasicVisualLexicon.NODE_LABEL_FONT_SIZE);
		mappingSizeLabel.addPoint(0.0d, nodeLabelSizeCA1);
		mappingSizeLabel.addPoint(CAMinRangeValueForRelevantZScores, nodeLabelSizeCA2);
		mappingSizeLabel.addPoint(CAMaxRangeValueForRelevantZScores, nodeLabelSizeCA3);

		vs.addVisualMappingFunction(mappingSizeLabel);

		// ***************************************************************
		// Define default style parameters
		// ********************************************************************
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_SHAPE);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_LABEL_COLOR);
		vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_TRANSPARENCY);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_SELECTED_PAINT);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_PAINT);
		vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_WIDTH);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_TRANSPARENCY, this.edgeTransparency);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SELECTED_PAINT, Color.CYAN);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_PAINT, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, new Double(2));

		// **************************************************************
		// Adapt
		// current
		// view
		// *************************************************************************

		// Add visual style to visual style manager
		vmm.addVisualStyle(vs);
		// set current style to RCA style
		vmm.setCurrentVisualStyle(vs);
		// updating network view
		myView.updateView();
	}

	/**
	 * Association of node name with their residue label
	 * 
	 * @param myNetwork
	 *            the network
	 * @param networkNodes
	 *            the nodes of the network
	 * @return the HashMap with the association of node SUID and residue labels
	 */

	private HashMap<Long, String> associateNodesSUIDResidueLabels(CyNetwork myNetwork, ArrayList<CyNode> networkNodes) {
		HashMap<Long, String> assocResidueLabelSUIDName = new HashMap<Long, String>(networkNodes.size());

		String attribute = getAttribute(myNetwork);
		for (int i = 0; i < networkNodes.size(); i++) {
			assocResidueLabelSUIDName.put(networkNodes.get(i).getSUID(),
					myNetwork.getRow(networkNodes.get(i)).get(attribute, String.class));
		}

		return assocResidueLabelSUIDName;

	}

	/**
	 * Get the name of the columns with residues
	 * 
	 * @param myNetwork
	 * @return "ResidueLabel" if the column exists, "name" if not
	 */

	private String getAttribute(CyNetwork myNetwork) {
		String attribute;
		if (myNetwork.getDefaultNodeTable().getColumn("ResidueLabel") != null)
			attribute = "ResidueLabel";
		else
			attribute = "name";

		return attribute;
	}

	public static void setNumberOfDigits(CyNetwork myNetwork, HashMap<Long, CyNode> nodes, HashMap<?, Double> hm,
			String columnName, int numberOfDigits) {
		Iterator<?> keySetIterator = hm.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Object key = keySetIterator.next();
			CyRow row = myNetwork.getRow(nodes.get(key));

			NumberFormat Myformat = NumberFormat.getInstance();
			Myformat.setMinimumFractionDigits(numberOfDigits); // Nb of Digits:
																// min
			Myformat.setMaximumFractionDigits(numberOfDigits); // Nb of Digits:
																// max
			String str = Myformat.format(hm.get(key)); // Formats with this
														// number of digits
			str = str.replace(',', '.');

			row.set(columnName, Double.parseDouble(str));
		}
	}

	@Override
	public void run(TaskMonitor arg0) throws Exception {

		System.out.println("Type of analysis:" + analysisType.getSelectedValue());

		CyNetwork myNetwork = this.appmanag.getCurrentNetwork();
		ArrayList<CyNode> nodes = (ArrayList<CyNode>) myNetwork.getNodeList();

		// associate the SUID of a node with the node
		HashMap<Long, CyNode> assocCyNodeSUID = new HashMap<Long, CyNode>(nodes.size());
		for (int i = 0; i < nodes.size(); i++) {
			assocCyNodeSUID.put(nodes.get(i).getSUID(), nodes.get(i));
		}

		// association of node name with their residue label
		HashMap<Long, String> assocResName = associateNodesSUIDResidueLabels(myNetwork, nodes);

		// create column "Residue"
		this.createResidueColumn(myNetwork, assocCyNodeSUID, assocResName);

		this.tm = arg0;

		// Average shortest Path length, removing each node
		if (analysisType.getSelectedValue().equals("Residue (ASPL change under removal of individual nodes) - RCA")) {

			tm.setTitle("Computing residue centrality...");

			// avspl value of original Network
			double avsplOri = avSplCalculation(myNetwork);
			// avspl values for each node
			HashMap<Long, Double> avspl = avsplForEachNode(myNetwork, nodes);
			// deltaLk values for each node
			HashMap<Long, Double> deltaLk = MathUtils.deltaLk(avsplOri, avspl);
			// average value of deltaLk values
			double averageDeltaLk = MathUtils.avgHashMapDouble(deltaLk);
			// standard deviation of deltaLk values
			double standardDeviation = MathUtils.stdDeviation(deltaLk, averageDeltaLk);
			// z-score values for each node
			ZScoreRCA = zScoreForEachNode(myNetwork, deltaLk, averageDeltaLk, standardDeviation);
			
			// create column "Z-score_RCA"
			String attributeName = zScoreRCAColumnName;
			if (myNetwork.getDefaultNodeTable().getColumn(zScoreRCAColumnName) == null)
				myNetwork.getDefaultNodeTable().createColumn(attributeName, Double.class, false);

			CA.setNumberOfDigits(myNetwork, assocCyNodeSUID, ZScoreRCA, attributeName, MathUtils.numberOfDigits);

			// updating network view
			createVisualStyle(assocCyNodeSUID, ZScoreRCA, attributeName, zScoreRCAColumnName);
		} else if (analysisType.getSelectedValue().equals("Betweenness - BCA")) {
			tm.setTitle("Computing betweenness centrality...");

			// calculates betweenness values
			HashMap<Long, Double> betweennessValues = betweennessCalculation(myNetwork);

			// average value of betweenness values
			double averageBetweenness = MathUtils.avgHashMapDouble(betweennessValues);
			// standard deviation of closeness values
			double standardDeviationBetweenness = MathUtils.stdDeviation(betweennessValues, averageBetweenness);

			ZScoreBCA = zScoreForEachNode(myNetwork, betweennessValues, averageBetweenness,
					standardDeviationBetweenness);

			// create column "Z-scoreBCA"
			String attributeName = zScoreBCAColumnName;
			if (myNetwork.getDefaultNodeTable().getColumn(zScoreBCAColumnName) == null)
				myNetwork.getDefaultNodeTable().createColumn(attributeName, Double.class, false);

			CA.setNumberOfDigits(myNetwork, assocCyNodeSUID, ZScoreBCA, attributeName, MathUtils.numberOfDigits);
			createVisualStyle(assocCyNodeSUID, ZScoreBCA, attributeName, zScoreBCAColumnName);
		} else if (analysisType.getSelectedValue().equals("Closeness - CCA")) {
			tm.setTitle("Computing closeness centrality...");
			// calculates closeness values
			HashMap<Long, Double> closenessValues = closenessCalculation(myNetwork);

			// average value of closeness values
			double averageCloseness = MathUtils.avgHashMapDouble(closenessValues);
			// standard deviation of closeness values
			double standardDeviationCloseness = MathUtils.stdDeviation(closenessValues, averageCloseness);

			ZScoreCCA = zScoreForEachNode(myNetwork, closenessValues, averageCloseness,
					standardDeviationCloseness);

			// create column "Z-score_CCA"
			String attributeName = zScoreCCAColumnName;
			if (myNetwork.getDefaultNodeTable().getColumn(zScoreCCAColumnName) == null)
				myNetwork.getDefaultNodeTable().createColumn(attributeName, Double.class, false);

			CA.setNumberOfDigits(myNetwork, assocCyNodeSUID, ZScoreCCA, attributeName, MathUtils.numberOfDigits);
			createVisualStyle(assocCyNodeSUID, ZScoreCCA, attributeName, zScoreCCAColumnName);
		}

		RINUtils.unselectAllNodesAndEdges(myNetwork);
	}

	/**
	 * 
	 * Gets all edges incident on the given node.
	 * 
	 * 
	 * 
	 * @param aNode
	 * 
	 *            CyNode , on which incident edges are to be found.
	 * 
	 * @return Array of edge indices, containing all the edges in the network
	 *         incident on
	 * 
	 *         <code>aNode</code>.
	 * 
	 */

	private List<CyEdge> getIncidentEdges(CyNetwork network, CyNode aNode) {

		return network.getAdjacentEdgeList(aNode, CyEdge.Type.ANY);

	}

	/**
	 * 
	 * Computes a normalization factor for node betweenness normalization.
	 * 
	 * 
	 * 
	 * @param count
	 * 
	 *            Number of nodes for which betweenness has been computed.
	 * 
	 * @return Normalization factor for node betweenness normalization.
	 * 
	 */

	protected double computeNormFactor(int count) {

		return (count > 2) ? (1.0 / ((count - 1) * (count - 2))) : 1.0;

	}

	/**
	 * Create Residue column
	 * 
	 * @param myNetwork
	 */

	private void createResidueColumn(CyNetwork myNetwork, HashMap<Long, CyNode> assocCyNodeSUID,
			HashMap<Long, String> assocResName) {

		String attribute = this.getAttribute(myNetwork);
		String attributeName = "Residue";
		if (myNetwork.getDefaultNodeTable().getColumn("Residue") == null)
			myNetwork.getDefaultNodeTable().createColumn(attributeName, String.class, false);
		else
			return;
		Iterator<Long> keySetIterator = assocCyNodeSUID.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Long key = keySetIterator.next();

			CyRow row = myNetwork.getRow(assocCyNodeSUID.get(key));
			String reslabel;
			if (attribute == "ResidueLabel") {
				reslabel = assocResName.get(key);

				reslabel = reslabel.replace(" ", "");
				if (myNetwork.getDefaultNodeTable().getColumn("ResChain") != null
						&& row.get("ResChain", String.class) != "_")
					reslabel = reslabel.substring(0, 1) + reslabel.substring(1, 3).toLowerCase() + reslabel.substring(3)
							+ "." + row.get("ResChain", String.class);
				else
					reslabel = reslabel.substring(0, 1) + reslabel.substring(1, 3).toLowerCase()
							+ reslabel.substring(3);
			} else {
				reslabel = assocResName.get(key);
			}
			row.set(attributeName, reslabel);
		}
	}
	
	//to retrieve results of centrality calculation in JSON format
	
	@SuppressWarnings("unchecked")
	@Override
	public <R> R getResults(Class<? extends R> type) {
		if (type.equals(String.class)) {
			if (analysisType.getSelectedValue().equals("Residue (ASPL change under removal of individual nodes) - RCA"))
				return (R) RINUtils.getJson(ZScoreRCA);
			else if (analysisType.getSelectedValue().equals("Betweenness - BCA"))
				return (R) RINUtils.getJson(ZScoreBCA);
			else if (analysisType.getSelectedValue().equals("Closeness - CCA"))
				return (R) RINUtils.getJson(ZScoreCCA);		
			return null;
		}
		else if (type.equals(JSONResult.class)) {
			JSONResult res = () -> {
				if (analysisType.getSelectedValue().equals("Residue (ASPL change under removal of individual nodes) - RCA"))
					return RINUtils.getJson(ZScoreRCA);
				else if (analysisType.getSelectedValue().equals("Betweenness - BCA"))
					return RINUtils.getJson(ZScoreBCA);
				else if (analysisType.getSelectedValue().equals("Closeness - CCA"))
					return RINUtils.getJson(ZScoreCCA);		
				
				return null;};
			return (R)(res);
		} else {
			return null;
		}
	}
	@Override 
	public List<Class<?>> getResultClasses() {
		return Arrays.asList(String.class, JSONResult.class);
	}
}
