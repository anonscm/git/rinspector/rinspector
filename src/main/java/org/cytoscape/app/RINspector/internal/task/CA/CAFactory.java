package org.cytoscape.app.RINspector.internal.task.CA;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.group.CyGroupFactory;
import org.cytoscape.group.CyGroupManager;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

/**
 * Task Factory that launches centrality analyses tasks
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class CAFactory extends AbstractTaskFactory {

	private final CyApplicationManager appmanag;
	private final CyNetworkManager netmgr;
	private final CyNetworkViewManager networkViewManager;
	private final VisualMappingManager vmm;
	private final CyNetworkFactory netFactory;
	private final CyNetworkViewFactory netViewFactory;
	private final CyNetworkNaming naming;
	private final CyNetworkTableManager netTableMgr;
	private final CyRootNetworkManager rootNetMgr;
	private final CyGroupManager groupMgr;
	private final CyGroupFactory groupFactory;
	private final RenderingEngineManager renderingEngineMgr;
	private final CyNetworkViewFactory nullNetworkViewFactory;
	private final VisualMappingFunctionFactory visMapFact;
	private final VisualStyleFactory visFactFactory;
	private final VisualMappingFunctionFactory vmfFactoryP;

	public CAFactory(final CyApplicationManager cyApplicationManager, final CyNetworkManager netmgr,
			final CyNetworkViewManager networkViewManager, final VisualMappingManager vmm,
			final CyNetworkFactory netFactory, final CyNetworkViewFactory netViewFactory, final CyNetworkNaming naming,
			final CyNetworkTableManager netTableMgr, final CyRootNetworkManager rootNetMgr,
			final CyGroupManager groupMgr, final CyGroupFactory groupFactory,
			final RenderingEngineManager renderingEngineMgr, final CyNetworkViewFactory nullNetworkViewFactory,
			final VisualMappingFunctionFactory visMapFact, final VisualStyleFactory visFactFactory,
			final VisualMappingFunctionFactory vmfFactoryP) {
		this.appmanag = cyApplicationManager;
		this.netmgr = netmgr;
		this.networkViewManager = networkViewManager;
		this.vmm = vmm;
		this.netFactory = netFactory;
		this.netViewFactory = netViewFactory;
		this.naming = naming;
		this.netTableMgr = netTableMgr;
		this.rootNetMgr = rootNetMgr;
		this.groupMgr = groupMgr;
		this.groupFactory = groupFactory;
		this.renderingEngineMgr = renderingEngineMgr;
		this.nullNetworkViewFactory = nullNetworkViewFactory;
		this.visFactFactory = visFactFactory;
		this.visMapFact = visMapFact;
		this.vmfFactoryP = vmfFactoryP;
	}

	@Override
	public TaskIterator createTaskIterator() {
		// creation of RINspector RCA calculation with a TaskIterator
		// (TaskMonitor)

		return new TaskIterator(new CA(appmanag, netmgr, networkViewManager, vmm, netFactory, netViewFactory, naming,
				netTableMgr, rootNetMgr, groupMgr, groupFactory, renderingEngineMgr, nullNetworkViewFactory, visMapFact,
				visFactFactory, vmfFactoryP));
	}

}
