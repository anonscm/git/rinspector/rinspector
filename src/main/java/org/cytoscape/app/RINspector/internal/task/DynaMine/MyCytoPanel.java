package org.cytoscape.app.RINspector.internal.task.DynaMine;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JPanel;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;

/**
 * 
 * Cytoscape Panel used for DynaMine
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class MyCytoPanel extends JPanel implements CytoPanelComponent {
	private static final long serialVersionUID = 8292806967891823933L;

	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	public String getIdentifier() {
		return this.getIdentifier();
	}

	public String getTitle() {
		return "DynaMine Prediction";
	}

	public Icon getIcon() {
		return null;
	}

	public Component getComponent() {
		return this;
	}

	public MyCytoPanel() {
		this.setVisible(true);
	}
}
