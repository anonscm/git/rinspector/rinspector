package org.cytoscape.app.RINspector.internal.task.tools;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.util.HashMap;
import java.util.Iterator;

/**
 * Class of static methods for some maths calculations
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class MathUtils {

	// number of digits used
	public static int numberOfDigits = 3;

	/**
	 * Compute deltaLk value for each node
	 * 
	 * @param avsplOri
	 *            avspl for the entire network
	 * @param avspl
	 *            hasmap of every avspl containing the avspl corresponding to
	 *            the node name
	 * @return hashmap containing the deltalk corresponding to the node name
	 */
	public static HashMap<Long, Double> deltaLk(double avsplOri, HashMap<Long, Double> avspl) {
		HashMap<Long, Double> deltaLk = new HashMap<Long, Double>(avspl.size());

		Iterator<Long> keySetIterator = avspl.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Long key = keySetIterator.next();
			deltaLk.put(key, Math.abs(avspl.get(key) - avsplOri));
		}
		return deltaLk;
	}

	/**
	 * Compute the average of the Double values of a HashMap<Object, Double>
	 * 
	 * @param hashMapDouble
	 *            hashmap that contains Double values that correspond to a
	 *            String key
	 * @return average value
	 */
	public static double avgHashMapDouble(HashMap<?, Double> hashMapDouble) {
		double total = 0;

		Iterator<?> keySetIterator = hashMapDouble.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Object key = keySetIterator.next();
			total += hashMapDouble.get(key);
		}

		return total / hashMapDouble.size();
	}

	/**
	 * Compute the standard deviation of the Double values of a HashMap<String,
	 * Double>
	 * 
	 * @param hashMapDouble
	 *            hashmap that contains Double values that correspond to a
	 *            String key
	 * @param avgHashMapDouble
	 *            average value of the Double values of the HashMap
	 * @return standard deviation value
	 */
	public static double stdDeviation(HashMap<?, Double> hashMapDouble, double avgHashMapDouble) {
		double sum = 0;
		Iterator<?> keySetIterator = hashMapDouble.keySet().iterator();

		while (keySetIterator.hasNext()) {
			Object key = keySetIterator.next();
			sum += Math.pow(hashMapDouble.get(key) - avgHashMapDouble, 2);
		}

		return Math.sqrt(sum / hashMapDouble.size());
	}

	/**
	 * Compute a Z-Score value
	 * 
	 * @param value
	 *            the value for which to calculate a Z-score
	 * @param average
	 *            average value of all the values
	 * @param standardDeviation
	 *            standard deviation of all the values
	 * @return zScore
	 */
	public static double zScore(double value, double average, double standardDeviation) {
		return (value - average) / standardDeviation;
	}

	/**
	 * Return the minimum and maximum value of a HasMap<String, Double>
	 * 
	 * @param hm
	 *            the HashMap from which to get the min and max
	 * @return an array which contains the minimum and maximum values (double)
	 */

	public static double[] getMinMax(HashMap<Long, Double> hm) {
		Iterator<Long> keySetIterator = hm.keySet().iterator();
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		while (keySetIterator.hasNext()) {
			Long key = keySetIterator.next();
			if (min > hm.get(key))
				min = hm.get(key);
			if (max < hm.get(key))
				max = hm.get(key);
		}

		double[] result = { min, max };

		return result;
	}
}
