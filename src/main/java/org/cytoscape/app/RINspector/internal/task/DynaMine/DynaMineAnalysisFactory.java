package org.cytoscape.app.RINspector.internal.task.DynaMine;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.session.CySessionManager;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

/**
 * Task Factory that launches DynaMine predictions tasks
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class DynaMineAnalysisFactory extends AbstractTaskFactory {
	private final CyApplicationManager cyApplicationManager;
	private final VisualMappingManager vmm;
	private final VisualStyleFactory visFactFactory;
	private final VisualMappingFunctionFactory visMapFact;
	private final VisualMappingFunctionFactory vmfFactoryP;
	private final CyServiceRegistrar registrar;
	private final CySwingApplication cytoscapeDesktopService;

	// list of all chains of the pdb
	public static ArrayList<String> chains;

	// DynaMine json key
	public final static String dynamineKey = "f60acb71ff6d3b1f1dfccdc14b1d820904a5c3ebf5f6453918bf63c7";

	public DynaMineAnalysisFactory(CyServiceRegistrar registrar, CySwingApplication cytoscapeDesktopService,
			CyApplicationManager cyApplicationManager, VisualMappingManager vmm, VisualStyleFactory visFactFactory,
			VisualMappingFunctionFactory visMapFact, VisualMappingFunctionFactory vmfFactoryP,
			CySessionManager sessMgr) {
		this.cyApplicationManager = cyApplicationManager;
		this.vmm = vmm;
		this.visFactFactory = visFactFactory;
		this.visMapFact = visMapFact;
		this.vmfFactoryP = vmfFactoryP;
		this.registrar = registrar;
		this.cytoscapeDesktopService = cytoscapeDesktopService;
	}

	public ArrayList<String> getAllchains() {
		CyNetwork network = this.cyApplicationManager.getCurrentNetwork();
		ArrayList<String> chain = new ArrayList<String>();

		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			if (!chain.contains(row.get("ResChain", String.class))) {
				chain.add(row.get("ResChain", String.class));
			}
		}
		return chain;
	}

	@Override
	public TaskIterator createTaskIterator() {
		// retrieve the chains for selection
		chains = getAllchains();

		// check if ResType, ResIndex and ResChain columns exist
		if (this.cyApplicationManager.getCurrentNetwork().toString().equals("cy:command_documentation_generation") ||(this.cyApplicationManager.getCurrentNetwork().getDefaultNodeTable().getColumn("ResType") != null
				&& this.cyApplicationManager.getCurrentNetwork().getDefaultNodeTable().getColumn("ResIndex") != null
				&& this.cyApplicationManager.getCurrentNetwork().getDefaultNodeTable().getColumn("ResChain") != null))
			return new TaskIterator(new DynaMineAnalysis(registrar, cytoscapeDesktopService, cyApplicationManager, vmm,
					visFactFactory, visMapFact, vmfFactoryP, dynamineKey));
		else {
			JOptionPane.showMessageDialog(null,
					"ResType, ResIndex and ResChain columns are not all present.\nAll the three are required for DynaMine prediction.\nResType column should contain the 3 letter code for each residue (e.g.: ARG).\nResIndex column should contain the index of each residue (e.g.: 153)\nResChain column should contain the chain identifier (e.g.: A)",
					"Error", JOptionPane.ERROR_MESSAGE);
			return new TaskIterator(new DynaMineAnalysisError());
		}
	}
}
