package org.cytoscape.app.RINspector.internal.task.help;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.awt.BorderLayout;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * Class that permits to open a help window
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class Help extends AbstractTask {

	public static final String release = "1.1.0";

	public static final String helpMessage = "***************************************\n" + "RINspector release "
			+ Help.release + "\n" + "***************************************\n\n"
			+ "RINspector integrates the calculation of Residue Interaction Network (RIN) centrality analyses with a DynaMine flexibility prediction of protein chains. Both analyses can be performed independently.\n\n"
			+ "Available centrality analyses are:\n"
			+ "- Residue Centrality Analysis (RCA)*, cf. del Sol A et al. Mol Syst Biol. 2006\n"
			+ "- Betweenness Centrality Analysis (BCA)\n" + "- Closeness Centrality Analysis (CCA)\n\n"
			+ "Every analysis assigns Z-scores to each individual residue.\n\n"
			+ "It is recommended to close StructureViz/Chimera prior to RCA in order to (significantly) decrease the calculation time.\n"
			+ "They can be launched again after the analysis complete.\n\n"
			+ "DynaMine predictions are retrieved from the DynaMine server (http://dynamine.ibsquare.be/).\n\n"
			+ "Usage:\n"
			+ "1. Generate a Residue Interaction Network (RIN) from a structure with structureViz/Chimera or import a RIN from a file.\n"
			+ "2. Select a centrality analysis or a DynaMine prediction in the menu of the app\n"
			+ "For DynaMine predictions, the 3 columns ResType, ResChain and ResIndex are required.  These are created automatically by structureViz/Chimera, but you will need to create them manually if your RIN does not contain them:\n"
			+ "     - ResType contains the 3 letters code of a residue (e.g.: ARG)\n"
			+ "     - ResIndex contains the index of the residue in a chain (e.g.: 153)\n"
			+ "     - ResChain contains the chain of the residue (e.g.: A)\n"
			+ "3. The visual style of the network is adapted in function of the calculated Z-score for centralities and S² values for DynaMine\n"
			+ "4. The DynaMine graph implements a right-click option window and a click-and-slide zoom box\n"
			+ "5. For mutations through DynaMine result panel, select one or several residues, type mutated residue(s) in one-letter format in the table and validate with \"enter\", then \"Run DynaMine\"\n\n"
			+ "*relies on the change in average shortest path length under removal of individual nodes\n\n"
			+ "The functionalities of this app can be accessed through the automation facilities provided by Cytoscape (>=3.6)\n\n"
			+ "This app was developed in the UGSF laboratory (http://ugsf.univ-lille1.fr). For bug report, please contact cytoscape-rinspector@univ-lille1.fr\n";

	public Help() {
		init();
	}

	/**
	 * Display help
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
	}

	private void init() {
		// create Help JFrame
		final JFrame frame = new JFrame("Help");
		final JEditorPane ed;

		ed = new JEditorPane();
		ed.setText(helpMessage);
		ed.setEditable(false);

		JScrollPane sp = new JScrollPane(ed);
		frame.setLayout(new BorderLayout());
		frame.getContentPane().add(sp, BorderLayout.CENTER);

		frame.setSize(900, 600);

		// set in the center of the screen
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
