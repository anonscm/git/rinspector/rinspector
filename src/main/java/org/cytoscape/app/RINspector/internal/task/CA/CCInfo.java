package org.cytoscape.app.RINspector.internal.task.CA;

import org.cytoscape.model.CyNode;

/**
 * 
 * Immutable storage of information on a connected component.
 * 
 * 
 * 
 * @author Yassen Assenov
 * 
 */

public class CCInfo {

	/**
	 * 
	 * Initializes a new instance of <code>CCInfo</code>.
	 * 
	 * 
	 * 
	 * @param aSize
	 *            Size of the connected component (number of nodes).
	 * 
	 * @param aNode
	 *            One of the nodes in the component.
	 * 
	 */

	public CCInfo(int aSize, CyNode aNode) {

		size = aSize;

		node = aNode;

	}

	/**
	 * 
	 * Gets the size of the connected component.
	 * 
	 * 
	 * 
	 * @return Number of nodes in the connected component.
	 * 
	 */

	public int getSize() {

		return size;

	}

	/**
	 * 
	 * Gets a node from the connected component.
	 * 
	 * 
	 * 
	 * @return Node belonging to this connected component.
	 * 
	 */

	public CyNode getNode() {

		return node;

	}

	/**
	 * 
	 * Number of nodes in the connected component.
	 * 
	 */

	private final int size;

	/**
	 * 
	 * One of the nodes in the connected component.
	 * 
	 */

	private final CyNode node;
}
