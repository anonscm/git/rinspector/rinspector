package org.cytoscape.app.RINspector.internal;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import static org.cytoscape.work.ServiceProperties.COMMAND_EXAMPLE_JSON;
import static org.cytoscape.work.ServiceProperties.COMMAND_SUPPORTS_JSON;

import java.util.HashMap;
import java.util.Properties;

import org.cytoscape.app.RINspector.internal.task.CA.CAFactory;
import org.cytoscape.app.RINspector.internal.task.DynaMine.DynaMineAnalysisFactory;
import org.cytoscape.app.RINspector.internal.task.help.HelpFactory;
import org.cytoscape.app.RINspector.internal.task.tools.RINUtils;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.group.CyGroupFactory;
import org.cytoscape.group.CyGroupManager;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.session.CySessionManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.work.ServiceProperties;
import org.osgi.framework.BundleContext;

/**
 * Initial Class to start the app
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class CyActivator extends AbstractCyActivator {

	public static final String getExampleCentrality() {
		HashMap<Long, Double> tmpRes = new HashMap<Long, Double>();
		tmpRes.put(12443l,0.256d);
		tmpRes.put(12858l,2.66d);
		tmpRes.put(11502l,0.152d);
		return RINUtils.getJson(tmpRes);
	}
	
	public static final String getExampleDynaMine() {
		HashMap<Long, Double> tmpRes = new HashMap<Long, Double>();
		tmpRes.put(12436l,0.655d);
		tmpRes.put(12828l,0.956d);
		tmpRes.put(16566l,0.554d);
		return RINUtils.getJson(tmpRes);
	}
	
	@Override
	public void start(BundleContext context) throws Exception {

		// get all the required services
		CyApplicationManager cyApplicationManager = getService(context, CyApplicationManager.class);
		CyNetworkManager netmgr = getService(context, CyNetworkManager.class);
		CyNetworkViewManager networkViewManager = getService(context, CyNetworkViewManager.class);
		VisualMappingManager vmm = getService(context, VisualMappingManager.class);
		CyNetworkFactory netFactory = getService(context, CyNetworkFactory.class);
		CyNetworkViewFactory netViewFactory = getService(context, CyNetworkViewFactory.class);
		CyNetworkNaming naming = getService(context, CyNetworkNaming.class);
		CyNetworkTableManager netTableMgr = getService(context, CyNetworkTableManager.class);
		CyRootNetworkManager rootNetMgr = getService(context, CyRootNetworkManager.class);
		CyGroupManager groupMgr = getService(context, CyGroupManager.class);
		CyGroupFactory groupFactory = getService(context, CyGroupFactory.class);
		RenderingEngineManager renderingEngineMgr = getService(context, RenderingEngineManager.class);
		CyNetworkViewFactory nullNetworkViewFactory = getService(context, CyNetworkViewFactory.class);
		VisualMappingFunctionFactory visMapFact = getService(context, VisualMappingFunctionFactory.class,
				"(mapping.type=continuous)");
		VisualMappingFunctionFactory vmfFactoryP = getService(context, VisualMappingFunctionFactory.class,
				"(mapping.type=passthrough)");
		VisualStyleFactory visFactFactory = getService(context, VisualStyleFactory.class);
		CySessionManager sessMgr = getService(context, CySessionManager.class);
		CyServiceRegistrar registrar = getService(context, CyServiceRegistrar.class);
		CySwingApplication cytoscapeDesktopService = getService(context, CySwingApplication.class);

		// creation of centrality menu item
		CAFactory CAaction = new CAFactory(cyApplicationManager, netmgr, networkViewManager, vmm, netFactory,
				netViewFactory, naming, netTableMgr, rootNetMgr, groupMgr, groupFactory, renderingEngineMgr,
				nullNetworkViewFactory, visMapFact, visFactFactory, vmfFactoryP);

		Properties rinFactProps = new Properties();
		rinFactProps.setProperty("preferredMenu", "Apps.RINspector");
		rinFactProps.setProperty("title", "Centrality Analysis");
		rinFactProps.setProperty("enableFor", "network"); // only accessible if
															// a network is
															// loaded
		rinFactProps.put(ServiceProperties.COMMAND_NAMESPACE, "rinspector");
		rinFactProps.put(ServiceProperties.COMMAND, "centrality");
		rinFactProps.put(ServiceProperties.COMMAND_DESCRIPTION, "Performs centrality analysis of a RIN");
		rinFactProps.put(ServiceProperties.COMMAND_LONG_DESCRIPTION, "Performs three types of centrality analyses on a Residue Interaction Network (RIN):\n"
				+ "- Residue Centrality Analysis (Average Shortest Path Length change under removal of individual nodes) - RCA\n"
				+ "- Betweenness Centrality Analysis - BCA\n"
				+ "- Closeness Centrality Analysis - CCA\n\n"
				+ "A Z-score is calculated for each node/residue for the chosen centrality and a column with these scores is added to the node table. Nodes color/size and label size are adapted in function of the Z-score value.\n"
				+ "Through automation, the results are also returned as a table that contains the calculated Z-Score for each node (SUID).\n");
		rinFactProps.setProperty(COMMAND_SUPPORTS_JSON, "true");
		rinFactProps.setProperty(COMMAND_EXAMPLE_JSON, getExampleCentrality());
		
		registerAllServices(context, CAaction, rinFactProps);
		
		// creation of Dynamine Prediction menu item
		DynaMineAnalysisFactory DynaAction = new DynaMineAnalysisFactory(registrar, cytoscapeDesktopService,
				cyApplicationManager, vmm, visFactFactory, visMapFact, vmfFactoryP, sessMgr);

		Properties dynaFactProps = new Properties();
		dynaFactProps.setProperty("preferredMenu", "Apps.RINspector");
		dynaFactProps.setProperty("title", "DynaMine Prediction");
		dynaFactProps.setProperty("enableFor", "network"); // only accessible if
															// a network is
															// loaded

		dynaFactProps.put(ServiceProperties.COMMAND_NAMESPACE, "rinspector");
		dynaFactProps.put(ServiceProperties.COMMAND, "dynamine");
		dynaFactProps.put(ServiceProperties.COMMAND_DESCRIPTION, "Performs DynaMine Prediction of a RIN");
		dynaFactProps.put(ServiceProperties.COMMAND_LONG_DESCRIPTION, "Performs DynaMine prediction for backbone flexibility of a protein for which a Residue Interaction Network has been generated.\n"
				+ "The table of the network should contain three columns to be able to run it: ResType, ResIndex and ResChain.\n"
				+ "- ResType column should contain the 3 letter code for each residue (e.g.: ARG)\n"
				+ "- ResIndex column should contain the index of each residue (e.g.: 153)\n"
				+ "- ResChain column should contain the chain identifier (e.g.: A)\n\n"
				+ "A S² value is calculated for each node/residue for the chosen chain and a column with these scores is added to the node table. Nodes color is adapted in function of the S² value.\n"
				+ "Through automation, the results are also returned as a table that contains the calculated S² score for each node (SUID).\n");
		
		dynaFactProps.setProperty(COMMAND_SUPPORTS_JSON, "true");
		dynaFactProps.setProperty(COMMAND_EXAMPLE_JSON, getExampleDynaMine());
		
		registerAllServices(context, DynaAction, dynaFactProps);

		// creation of the Help menu item
		HelpFactory helpAction = new HelpFactory();

		Properties helpProps = new Properties();
		helpProps.setProperty("preferredMenu", "Apps.RINspector");
		helpProps.setProperty("title", "Help");
		helpProps.setProperty("enableFor", "always");

		registerAllServices(context, helpAction, helpProps);
	}
}