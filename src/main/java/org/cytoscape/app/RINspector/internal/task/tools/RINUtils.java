package org.cytoscape.app.RINspector.internal.task.tools;

/* 
* Copyright or © or Copr. National Center for Scientific Research or CNRS
* Contributor(s): Guillaume Brysbaert, Kevin Lorgouilloux and Marc Lensink (2018)
* 
* Contact: cytoscape-rinspector _[At]_ univ-lille1.fr
* 
* This software is a computer program whose purpose is to perform centrality analyses 
* on residue interaction networks and predict flexibility of protein backbones.
* 
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use, 
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* 
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited liability. 
* 
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
*/

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JTable;

import org.cytoscape.app.RINspector.internal.task.CloneNetworkTask;
import org.cytoscape.app.RINspector.internal.task.CA.SifWriter;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.work.TaskMonitor;

import com.google.gson.Gson;

/**
 * Provide tools to perform general tasks
 * 
 * @author Kevin Lorgouilloux, Guillaume Brysbaert
 *
 */

public class RINUtils {

	/**
	 * List all nodes in console
	 * 
	 * @param network
	 *            Network containing nodes
	 */
	public static void listOfNodes(CyNetwork network) {
		java.util.List<CyNode> nodes = network.getNodeList();
		for (int i = 0; i < nodes.size(); i++) {
			System.out.println(network.getRow(nodes.get(i)).get(CyNetwork.NAME, String.class));

		}
	}

	/**
	 * Return a CyNetwork object which is the exact copy of the orgNet CyNetwork
	 * 
	 * @param orgNet
	 *            Original network
	 * @return exact copy of the original network
	 */
	public static CyNetwork getNetworkCopy(CloneNetworkTask clone) {
		return clone.cloneNetwork(clone.getParentNetwork());
	}

	/**
	 * Remove node from network
	 * 
	 * @param network
	 *            network which contain the node
	 * @param node
	 *            the node to remove
	 * @return true if the operation succeed, false if the node isn't in the
	 *         network
	 */
	public static boolean removeNode(CyNetwork network, CyNode node) {
		if (network.containsNode(node)) {
			ArrayList<CyNode> nodes = new ArrayList<CyNode>();
			nodes.add(node);
			network.removeNodes(nodes);
			return true;
		} else
			return false;
	}

	/**
	 * Return a set of neighbors of aNode
	 * 
	 * @param aNode
	 * @param network
	 * @return
	 */
	public static Set<CyNode> getNeighbors(CyNode aNode, CyNetwork network) {
		return getNeighbors(network, aNode, getIncidentEdges(aNode, network));
	}

	/**
	 * Return a set of neighbors of aNode
	 * 
	 * @param aNetwork
	 * @param aNode
	 * @param aIncEdges
	 * @return
	 */
	public static Set<CyNode> getNeighbors(CyNetwork aNetwork, CyNode aNode, java.util.List<CyEdge> aIncEdges) {
		Set<CyNode> neighborsSet = new HashSet<CyNode>();
		Iterator<CyEdge> i$ = aIncEdges.iterator();
		do {
			if (!i$.hasNext())
				break;
			CyEdge e = i$.next();
			CyNode sourceNode = e.getSource();
			if (sourceNode != aNode) {
				neighborsSet.add(sourceNode);
			} else {
				CyNode targetNode = e.getTarget();
				if (targetNode != aNode)
					neighborsSet.add(targetNode);
			}
		} while (true);
		return neighborsSet;
	}

	@SuppressWarnings("unused")
	private static java.util.List<CyEdge> getIncidentEdges(CyNode aNode, CyNetwork network) {
		return network.getAdjacentEdgeList(aNode,
				false ? org.cytoscape.model.CyEdge.Type.INCOMING : org.cytoscape.model.CyEdge.Type.ANY);
	}

	@SuppressWarnings("unused")
	private static int countNeighborsIn(Set<CyNode> aSet, CyNode aNode, CyNetwork network, CyApplicationManager cam) {
		Set<CyNode> nbs = getNeighbors(cam.getCurrentNetwork(), aNode, getIncidentEdges(aNode, network));
		nbs.retainAll(aSet);
		return nbs.size();
	}

	/**
	 * Write the network in the SIF file
	 * 
	 * @param network
	 *            network to write
	 * @param name
	 *            name of the SIF file
	 */
	public static void writeSIFfile(CyNetwork network, String name) {
		OutputStream output;
		try {
			output = new FileOutputStream("./temp/" + name + ".sif");
			SifWriter s = new SifWriter(output, network);
			System.out.println("SifWriter created");
			try {
				s.run(null);
				output.close();
			} catch (Exception e) {
				System.out.println("Run doesn't work");
			}

		} catch (FileNotFoundException e) {
			System.out.println("Output doesn't work.");
		}
	}

	/**
	 * Send request to URL
	 * 
	 * @param targetURL
	 *            url to send request
	 * @param urlParameters
	 *            url post parameters
	 * @return string response from the server
	 */
	public static String executePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;
		try {
			// Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/**
	 * Add association between Amino-acids 3-letters code with 1-letter code
	 * 
	 * @param aa
	 *            the HashMap which contains the associations
	 * @return the completed HashMap
	 */
	public static HashMap<String, Character> initHashMap(HashMap<String, Character> aa) {
		aa.put("ALA", 'A');
		aa.put("CYS", 'C');
		aa.put("ASP", 'D');
		aa.put("GLU", 'E');
		aa.put("PHE", 'F');
		aa.put("GLY", 'G');
		aa.put("HIS", 'H');
		aa.put("ILE", 'I');
		aa.put("LYS", 'K');
		aa.put("LEU", 'L');
		aa.put("MET", 'M');
		aa.put("ASN", 'N');
		aa.put("PYL", 'O');
		aa.put("PRO", 'P');
		aa.put("GLN", 'Q');
		aa.put("ARG", 'R');
		aa.put("SER", 'S');
		aa.put("THR", 'T');
		aa.put("SEC", 'U');
		aa.put("VAL", 'V');
		aa.put("TRP", 'W');
		aa.put("TYR", 'Y');

		return aa;
	}

	/**
	 * Print progress in taskmonitor
	 * 
	 * @param status
	 *            status of current job
	 * @param flag
	 */

	public static void printProgress(String status, boolean flag, TaskMonitor tm) {
		boolean running = false;
		if (status.equals("queued")) {
			if (flag)
				tm.setTitle("Waiting to be processed.");
			else
				tm.setTitle("The request has been submitted");
			running = false;
		} else if (status.equals("running")) {
			if (!running) {
				running = true;
				tm.setTitle("The request is being processed...");
			} else
				tm.setTitle("The request is being processed...");
		} else if (status.equals("completed")) {
			if (!running)
				tm.setTitle("The request is being processed...");
			tm.setTitle("done.");
		} else if (status.equals("error")) {
			tm.setTitle("Error while processing the request :");
		}
	}

	/**
	 * Mutate one residue with a new residue
	 * 
	 * @param sequence
	 * @param residue
	 *            position of the residue to mutate
	 * @param newRes
	 *            new residue
	 * @return The mutated sequence
	 */
	public static String mutateSequence(String sequence, int residue, char newRes) {
		return sequence.substring(0, residue) + newRes + sequence.substring(residue + 1);
	}

	/**
	 * Clear all data in a JTable
	 * 
	 * @param table
	 */
	public static void clearTable(JTable table) {
		for (int i = 0; i < table.getRowCount(); i++)
			for (int j = 0; j < table.getColumnCount(); j++) {
				table.setValueAt("", i, j);
			}
	}

	/**
	 * Download file from url
	 * 
	 * @param host
	 *            url of file
	 * @param givenPath
	 *            flag to define if save path is given or not
	 * @param pathGiven
	 *            save path
	 */
	public static void saveFile(String host, boolean givenPath, File pathGiven) {
		InputStream input = null;
		FileOutputStream writeFile = null;

		try {
			URL url = new URL(host);
			URLConnection connection = url.openConnection();
			int fileLength = connection.getContentLength();

			if (fileLength == -1) {
				System.out.println("Invalide URL or file.");
				return;
			}

			input = connection.getInputStream();
			String fileName = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);

			if (!givenPath) {
				File path = new File(".");
				JFileChooser dialogue = new JFileChooser(path);
				dialogue.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				if (dialogue.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					path = dialogue.getSelectedFile();
				}

				fileName = path.getPath() + File.separator + fileName;
			} else
				fileName = pathGiven.getPath() + File.separator + fileName;

			writeFile = new FileOutputStream(fileName);
			byte[] buffer = new byte[1024];
			int read;

			while ((read = input.read(buffer)) > 0)
				writeFile.write(buffer, 0, read);
			writeFile.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writeFile.close();
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Unselect all nodes of a network
	 * 
	 * @param network
	 *            the network for which to unselect all nodes
	 */

	public static void unselectAllNodesAndEdges(CyNetwork network) {
		for (CyNode node : network.getNodeList()) {
			CyRow row = network.getRow(node);
			row.set(CyNetwork.SELECTED, false);
		}
		for (CyEdge edge : network.getEdgeList()) {
			CyRow row = network.getRow(edge);
			row.set(CyNetwork.SELECTED, false);
		}
	}
	
	/**
	 * Converts to JSON
	 * @param result result to convert to JSON
	 * @return result converted to JSON as String
	 */
	public static final String getJson(HashMap<Long, Double> result) {
		return new Gson().toJson(result);
	}
	
	/**
	 * Converts to JSON
	 * @param result result to convert to JSON
	 * @return result converted to JSON as String
	 */
	
	public static final String getJson(String result) {
		return new Gson().toJson(result);
	}
	
}
